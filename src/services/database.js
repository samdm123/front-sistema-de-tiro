export const items = [
  {
      label: 'Somar Modalidades:',
      select: 'modalities',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Bloquear no Site:',
      select: 'block-site',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Nome Atleta:',
      select: 'name-athlete',
      numberValue_0:'0',
      numberValue_1:'1',
      numberValue_2:'2',
      contentValue_0:'Nome Completo',
      contentValue_1:'1° Nome',
      contentValue_2:'1° e 2° Nome',
  },
  {
      label: '1/2 Peso:',
      select: 'weight-athlete',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Habilitar Tipos de Peso',
      select: 'weight-type',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Nome Modelo Europeu (Sobrenome, Nome):',
      select: 'name-model-europe',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Bloquear Recálculo:',
      select: 'block-recalculation',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Mostrar Estado do Atleta:',
      select: 'athlete-state',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },
  {
      label: 'Imprimir CPF do Atleta na Súmula:',
      select: 'athlete-cpf',
      numberValue_0:'0',
      numberValue_1:'1',
      contentValue_0:'Não',
      contentValue_1:'Sim',
  },

]

export const selectOptions = [
    {
        value: 'AGENDAMENTO DE TESTE DE CAPACIDADE TÉCNICA PARA MANUSEIO DE ARMA DE FOGO',
    },
    {
        value: 'Autorização de Guarda de Acervo',
    },
    {
        value: 'Autorização Para Aquisição De Arma De Fogo Na Indústria (ATIRADOR Desportivo) - 93/Colog/I-1',
    },
    {
        value: ' Autorização Para Aquisição De Arma De Fogo Na Indústria (CAÇADOR) – 93/Colog/I-3',
    },
    {
        value: 'Capa de Processo',
    },
    {
        value: ' Capa processo de Registro de Arma',
    },
    {
        value: 'Certificado de Participação em Prova',
    },
    {
        value: 'Certificado de Participação em Prova - Paisagem',
    },
    {
        value: 'Certificado de Participação em Prova/Modalidade',
    },
    {
        value: 'Certificado de Participação em Prova/Modalidade - Paisagem',
    },
    {
        value: 'Convite',
    },
    {
        value: 'Convite - Modelo 02',
    },
    {
        value: 'CORREÇÃO AO CERTIFICADO DE REGISTRO - 27º B LOG 11/2019',
    },
    {
        value: 'Declaração Bolsa Atleta',
    },
    {
        value: 'DECLARAÇÃO DE ARMAZENAGEM DE PCE',
    },
    {
        value: 'Declaração de Armazenamento de Arma de Fogo',
    },
    {
        value: 'Declaração de Autorização para Avaliação',
    },
]

export const itemsAddChampionship = [
    {
        label: 'Somar Modalidades:',
        select: 'modalities',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Bloquear no Site:',
        select: 'block-site',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Nome Atleta:',
        select: 'name-athlete',
        numberValue_0:'0',
        numberValue_1:'1',
        numberValue_2:'2',
        contentValue_0:'Nome Completo',
        contentValue_1:'1° Nome',
        contentValue_2:'1° e 2° Nome',
    },
    {
        label: '1/2 Peso:',
        select: 'weight-athlete',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Habilitar Tipos de Peso',
        select: 'weight-type',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Nome Modelo Europeu (Sobrenome, Nome):',
        select: 'name-model-europe',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Bloquear Recálculo:',
        select: 'block-recalculation',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Mostrar Estado do Atleta:',
        select: 'athlete-state',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
    {
        label: 'Imprimir CPF do Atleta na Súmula:',
        select: 'athlete-cpf',
        numberValue_0:'0',
        numberValue_1:'1',
        contentValue_0:'Não',
        contentValue_1:'Sim',
    },
 
]


export const itemsAddPhase = [
    {  
        label: 'Converter em Local:',
        forNameId: 'convert-local', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Gerar Boleto:',
        forNameId: 'generate-ticket', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Bloquear Resultado:',
        forNameId: 'block-result', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Cadastro Completo:',
        forNameId: 'register-complete', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Pode Descartar:',
        forNameId: 'discard', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Dobrar Etapa:',
        forNameId: 'double-phase', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Vincular Arma:',
        forNameId: 'link-weapon', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Integrar APP:',
        forNameId: 'integrate-app', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Integrar Fluxo de Caixa:',
        forNameId: 'integrate-flowBox', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Habilitar Permiso:',
        forNameId: 'permission', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Resultado Parcial:',
        forNameId: 'result-partial', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Mostrar Status Pagamento:',
        forNameId: 'show-status-pay', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Bloquear Recálculo:',
        forNameId: 'block-recalculate', 
        op01: 'Não',
        op02: 'Sim'
    },
    {  
        label: 'Bloquear Convidado:',
        forNameId: 'block-visit', 
        op01: 'Não',
        op02: 'Sim'
    },

    
]

export const itemsTags = [
    {
        label: 'Exibir Nome',
        forNameId: 'showName',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir Matrícula',
        forNameId: 'showRegistration',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir CPF',
        forNameId: 'showCPF',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir Modalidade/Divisão',
        forNameId: 'showModality',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir Esquadra',
        forNameId: 'showSquad',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir Arma',
        forNameId: 'showWeapon',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir Marca',
        forNameId: 'showBrand',
        op01: 'Não',
        op02: 'Sim'
    },
    {
        label: 'Exibir Calibre',
        forNameId: 'showCaliber',
        op01: 'Não',
        op02: 'Sim'
    },
]

export const checkItems = [
    {
        label: 'Inadimplente',
        forNameId: 'defaulter',
    },
    {
        label: 'Mensalidade/Anuidade',
        forNameId: 'annuity',
    },
    {
        label: 'CR',
        forNameId: 'cr',
    },
    {
        label: 'Documentação',
        forNameId: 'documentation',
    },
    {
        label: 'Acervo',
        forNameId: 'collection',
    },
    {
        label: 'Clube Inadimplente',
        forNameId: 'default-club',
    },
]