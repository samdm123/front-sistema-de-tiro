// import Dashboard from "./components/Dashboard";
import Header from "./components/Header";

import {Main} from "./components/Main"

import {ProviderHideShowMenu} from './hooks/useHideShowMenu'

import {Global} from './styles/global'

function App() {
  return (
    <>  
      <ProviderHideShowMenu>

        <Header/>
        <Main/>
    
        <Global/>

      </ProviderHideShowMenu>
    </>
  );
}

export default App;
