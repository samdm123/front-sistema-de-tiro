import {useState, useContext, createContext} from 'react'

const contextHideShowMenu = createContext()


export function ProviderHideShowMenu({children}) {

  const [hideShowMenu, setHideShowMenu] = useState(false)

  return(
    <contextHideShowMenu.Provider value={{ hideShowMenu, setHideShowMenu}}>
      {children}
    </contextHideShowMenu.Provider>
  )
}

export function useContextMenu(){
  const context = useContext(contextHideShowMenu)

  const {hideShowMenu, setHideShowMenu} = context

  return {hideShowMenu, setHideShowMenu} 
}