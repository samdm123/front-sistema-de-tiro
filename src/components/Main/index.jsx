import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import { Sidebar } from "../Sidebar";
import { Home } from '../../pages/Home';
import {Statements} from "../../pages/Secretary/Documents/Statements";
import GeneralModalities from "../../pages/Competitions/GeneralModalities"
import Championship from "../../pages/Competitions/Championship/index"
import AddChampionship from "../../pages/Competitions/Championship/AddChampionship";
import Phases from '../../pages/Competitions/Phases'
import SettingsCompetitions from '../../pages/Competitions/Phases/SettingsCompetitions'
import HangTags from '../../pages/Competitions/Phases/HangTags'
import AppBackup from "../../pages/Competitions/Phases/AppBackup";
import ReportPhases from "../../pages/Competitions/Phases/ReportPhases";
import ReportAthlete from "../../pages/Competitions/Phases/ReportAthlete";
import AddPhase from "../../pages/Competitions/Phases/AddPhase";

import {ContainerMain} from './styles'

export  function Main() {
  return (
    <ContainerMain>
      <Sidebar/>

        <Router>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/declaracoes" element={<Statements/>} />
            <Route path="/modalidades-gerais" element={<GeneralModalities/>} />
            <Route path="/campeonatos" element={<Championship/>} />
            <Route path="/campeonatos/adicionar" element={<AddChampionship/>} />
            <Route path="/etapas" element={<Phases/>} />
            <Route path="/etapas/configuracoes/competicao" element={<SettingsCompetitions/>} />
            <Route path="/etapas/configuracoes/competicao" element={<SettingsCompetitions/>} />
            <Route path="/etapas/etiquetas" element={<HangTags/>} />
            <Route path="/etapas/backup-do-aplicativo" element={<AppBackup/>} />
            <Route path="/etapas/relatorio-de-etapas" element={<ReportPhases/>} />
            <Route path="/etapas/relatorio-do-atleta" element={<ReportAthlete/>} />
            <Route path="/etapas/relatorio-do-atleta" element={<ReportAthlete/>} />
            <Route path="/etapas/adicionar-etapa" element={<AddPhase/>} />
          </Routes>
        </Router>

    </ContainerMain>
  );
}
