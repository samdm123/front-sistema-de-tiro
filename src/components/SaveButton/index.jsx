import { SaveButton } from "./styles"

export default function SaveBtn() {
    return (
        <SaveButton type="submit">
            Salvar
        </SaveButton>
    )
}