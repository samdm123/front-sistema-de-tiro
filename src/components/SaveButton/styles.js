import styled from "styled-components";

export const SaveButton = styled.button `
        border: 0;
        padding: .6rem 1.3rem;
        color: white;
        cursor: pointer;
        text-transform: uppercase;
        transition: .4s;
        background: #65B951;


        :hover {
            filter: brightness(110%);
        }

`