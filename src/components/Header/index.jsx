import {GiHamburgerMenu} from 'react-icons/gi'
import {IoMdNotifications} from 'react-icons/io'
import {GoPerson} from 'react-icons/go'
import {useContextMenu} from '../../hooks/useHideShowMenu'

import {ContainerHeader} from './styles.js'


export default function Header(){
    const {hideShowMenu, setHideShowMenu} = useContextMenu()

    function handleHideAndShowMenu(){
        console.log(hideShowMenu)
    }
    
    return(
        <ContainerHeader >
            <div className="headerRight">
                <a href="#"><h1>LOGO</h1></a>
                <a className="menu" href="#" onClick={handleHideAndShowMenu}>
                    <GiHamburgerMenu color="#fff"/>
                </a>
            </div>
            <nav className="navMenu">
                <ul>
                    <li className="notification"><a href="#"> <IoMdNotifications color="#fff"/> NOTIFICAÇÕES</a></li>
                    <li className="user"><a href="#"> <GoPerson color="#fff"/> ITACOATIARA CLUBE DE TIRO - ICT</a></li>
                </ul>
            </nav>
            

        </ContainerHeader>
    )
}