import styled from 'styled-components'

export const ContainerHeader = styled.header`
  width: 100%;
  height: 2.6rem;
  background-color: #11a9cced;
  display: flex;
  align-items: center;
  justify-content: space-between;

  .containerHeader a {
    text-decoration: none;
    color: #fff;
    font-weight: 300;
  }

  .headerRight {
    margin-left: 3rem;
    display: flex;
    align-items: center;
    gap: 4rem;
  }

  .menu {
    padding: 0.4rem;
    transition: 0.5s;
  }

  .menu:hover {
    background: rgba(0, 0, 0, 0.171);
  }

  .navMenu ul {
    display: flex;
    align-items: center;
    margin-right: 4rem;
    gap: 4rem;
  }

  .navMenu ul li {
    list-style: none;
  }

  .navMenu ul li a {
    text-decoration: none;
  }

  .notification a {
    font-size: 0.9rem;
    padding: 0.1rem;

    display: flex;
    align-items: center;
    gap: 0.2rem;

    transition: 0.5s;
  }

  .notification a:hover {
    background: rgba(0, 0, 0, 0.171);
  }

  .I-notification {
    width: 2rem;
    margin-bottom: 0.4rem;
  }

  .I-user {
    width: 1.6rem;
    margin-bottom: 0.1rem;
  }

  .user a {
    font-size: 0.9rem;
    padding: 0.5rem;
    margin-bottom: 0.1rem;

    display: flex;
    align-items: center;
    gap: 0.3rem;

    transition: 0.5s;
  }

  .user a:hover {
    background: rgba(0, 0, 0, 0.171);
  }
`
