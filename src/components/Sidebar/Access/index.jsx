import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { AccordionDefault } from '../../AccordioDefault';

import {IoIosPeople} from "react-icons/io"

export function Access(){
  return(
     
    <Accordion>
    <AccordionSummary
      expandIcon={<ExpandMoreIcon />}
      aria-controls="panel2a-content"
      id="panel2a-header"
    >
      <Typography component={'span'} variant={'body2'}> <IoIosPeople color='#525c5a'/> Acessos</Typography>
    </AccordionSummary>
    <AccordionDetails>
      <Typography component={'span'} variant={'body2'}>
          <AccordionDefault title="Clube de Federação"/>
          <AccordionDefault title="Despachantes"/>
          <AccordionDefault title="Exércitos"/>
          <AccordionDefault title="Últimos Acessos"/>

      </Typography>
    </AccordionDetails>
  </Accordion>
  )
}