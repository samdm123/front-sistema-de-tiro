import * as React from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { AccordionDefault } from '../../AccordioDefault'
import { SiBookstack } from 'react-icons/si'
import { BsNewspaper } from 'react-icons/si'

import { HiDocumentText } from 'react-icons/hi'

export function Secretary() {
  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Typography component={'span'} variant={'body2'}>
          <SiBookstack color="#803819" />
          Secretaria
        </Typography>
      </AccordionSummary>

      <AccordionDetails>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography component={'span'} variant={'body2'}>Documentos</Typography>
          </AccordionSummary>
          <AccordionDetails>

            <AccordionDefault 
              title="Declarações" 
              icon={<HiDocumentText />} 
              path="/declaracoes"
            />

            <AccordionDefault title="Tipos de Documentos"  />

            <AccordionDefault title="Documentos Word" />

            <AccordionDefault title="Templates Word" />
            <AccordionDefault title="Ofícios" />
            <AccordionDefault title="Outros" />
            <AccordionDefault title="Lotes" />
            <AccordionDefault title="Categorias" />
            <AccordionDefault title="Documentos de Apoio" />
            <AccordionDefault title="Configurações" />
          </AccordionDetails>
        </Accordion>

        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography component={'span'} variant={'body2'}>Despachantes</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Serviços" />

                <AccordionDefault title="Solicitações" />

                <AccordionDefault title="Status" />
            </Typography>
          </AccordionDetails>
        </Accordion>

        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography component={'span'} variant={'body2'}>Fichas Eletrônicas</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography component={'span'} variant={'body2'}>
              <AccordionDefault title="Fichas" />

              <AccordionDefault title="Itens" />

              <AccordionDefault title="Lançamentos" />
            </Typography>
          </AccordionDetails>
        </Accordion>

        <AccordionDefault title="Cursos" />
        <AccordionDefault title="Prestação de Contas" />
        <AccordionDefault title="Laudos" />
        <AccordionDefault title="Acessos a Entidade" />

        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography component={'span'} variant={'body2'}>Controle de Visitas</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography component={'span'} variant={'body2'}>
              <AccordionDefault title="Visitas" />

              <AccordionDefault title="Finalidades" />
            </Typography>
          </AccordionDetails>
        </Accordion>

        <AccordionDefault title="Acervo da Entidade" />
        <AccordionDefault title="Acervo Avulso" />
        <AccordionDefault title="Termos/Notificaçõe" />
        <AccordionDefault title="Agendamentos" />
      </AccordionDetails>
    </Accordion>
  )
}
