import * as React from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { AccordionDefault } from '../../AccordioDefault'

import { FiDollarSign } from 'react-icons/fi'

export function Financier() {
  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Typography component={'span'} variant={'body2'}>
          <FiDollarSign color='#159f0b'/>
          Finaceiro
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography component={'span'} variant={'body2'}>
          <AccordionDefault title="Boletos" />

          <AccordionDefault title="Fluxo de Caixa" />

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography component={'span'} variant={'body2'}>Notas Fiscais</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Notas Emitidas" />

                <AccordionDefault title="Tributos" />

                <AccordionDefault title="Serviços" />
                <AccordionDefault title="Empresas" />
                <AccordionDefault title="Certificados" />
                <AccordionDefault title="Configurações" />
                <AccordionDefault title="Notas de Entrada" />
                <AccordionDefault title="Trasportadoras" />
                <AccordionDefault title="Relatórios" />
              </Typography>
            </AccordionDetails>
          </Accordion>

          <AccordionDefault title="Plano de Contas" />

          <AccordionDefault title="Contas a Pagar" />

          <AccordionDefault title="Contas Bancárias" />
          <AccordionDefault title="Extrato de Cartões" />
          <AccordionDefault title="Saques PagHiper" />
          <AccordionDefault title="Retorno Bancario" />
          <AccordionDefault title="Remessa Bancária" />
          <AccordionDefault title="Tipos de Entradas" />
          <AccordionDefault title="Fornecedores/Clientes" />

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography component={'span'} variant={'body2'}>Relatórios</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <AccordionDefault title="Adimplentes" />

              <AccordionDefault title="Inadimplentes" />

              <AccordionDefault title="Unificados" />

              <AccordionDefault title="Forncedores" />
              <AccordionDefault title="Cobrança de Boletos" />
            </AccordionDetails>
          </Accordion>

          <AccordionDefault title="Cobrança Recorrente" />
          <AccordionDefault title="GalaxPay" />
        </Typography>
      </AccordionDetails>
    </Accordion>
  )
}
