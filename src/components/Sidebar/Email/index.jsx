import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { AccordionDefault } from '../../AccordioDefault';

import {AiOutlineMail} from "react-icons/ai"

export function Email(){
  return(
     
    <Accordion>
    <AccordionSummary
      expandIcon={<ExpandMoreIcon />}
      aria-controls="panel2a-content"
      id="panel2a-header"
    >
      <Typography component={'span'} variant={'body2'}> <AiOutlineMail color="#b1b1b1"/> Email</Typography>
    </AccordionSummary>
    <AccordionDetails>
      <Typography component={'span'} variant={'body2'}>
          <AccordionDefault title="Campanhas"/>
          <AccordionDefault title="Envios"/>
          <AccordionDefault title="Configurações"/>
          <AccordionDefault title="Lista Negra"/>

      </Typography>
    </AccordionDetails>
  </Accordion>
  )
}