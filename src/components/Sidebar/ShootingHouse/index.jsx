import * as React from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { AccordionDefault } from '../../AccordioDefault'

import { FaLaptop } from 'react-icons/fa'

export function ShootingHouse() {
  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Typography component={'span'} variant={'body2'}>
          {' '}
          <FaLaptop color="#8ba6ac" /> Shooting House
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography component={'span'} variant={'body2'}>
          <AccordionDefault title="Chamadas" />
          <AccordionDefault title="Chamados WhatsApp" />
          <AccordionDefault title="Financeiros" />
          <AccordionDefault title="Auditoria" />
          <AccordionDefault title="Notificações/Novidades" />
          <AccordionDefault title="Termos Aceitos" />
          <AccordionDefault title="Treinamento" />
        </Typography>
      </AccordionDetails>
    </Accordion>
  )
}
