import * as React from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { AccordionDefault } from '../../AccordioDefault'
import {BsCart4} from "react-icons/bs"

export function Store() {
  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Typography component={'span'} variant={'body2'}>
            <BsCart4/>
            Loja
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography component={'span'} variant={'body2'}>

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography component={'span'} variant={'body2'}>Pedidos</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Pedido" />
                <AccordionDefault title="Devolução" />
              </Typography>
            </AccordionDetails>

            
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography component={'span'} variant={'body2'}>Produtos</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Produtos" />
                <AccordionDefault title="Categoria" />
                <AccordionDefault title="Marca" />
                <AccordionDefault title="Tabela de preços" />
              </Typography>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography component={'span'} variant={'body2'}>Estoque</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Depósito" />
                <AccordionDefault title="Confecção" />
                <AccordionDefault title="Leitura XML" />
                <AccordionDefault title="Solicitação de Tranasferência" />
                <AccordionDefault title="Devolução Fornecedor" />
              </Typography>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography component={'span'} variant={'body2'}>Controle de Armas</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Armas" />
                <AccordionDefault title="Status" />
              </Typography>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography>Ordens de Serviço</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography component={'span'} variant={'body2'}>
                <AccordionDefault title="Ordem de Serviço" />
                <AccordionDefault title="Tipos" />
              </Typography>
            </AccordionDetails>
          </Accordion>

                <AccordionDefault title="Promoções" />
                <AccordionDefault title="Relatórios" />
                <AccordionDefault title="Configurações" />
                <AccordionDefault title="Tags" />
                <AccordionDefault title="Etiquetas" />
        </Typography>
      </AccordionDetails>
    </Accordion>
  )
}
