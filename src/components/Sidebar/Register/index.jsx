import * as React from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { AccordionDefault } from '../../AccordioDefault';
import {AiOutlineEdit} from "react-icons/ai"


export function Register() {
  return (
    <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
         
        >
          <Typography component={'span'} variant={'body2'}>
            <AiOutlineEdit color="##f58d22"/>
             Cadastros
          </Typography>

        </AccordionSummary>

        <AccordionDetails>
          <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header"
              >
                 <Typography component={'span'} variant={'body2'}>Atletas</Typography>

              </AccordionSummary>
              <AccordionDetails>
                  
                  <AccordionDefault title="Filiados"/>

                  <AccordionDefault title="Dependentes"/>

                  <AccordionDefault title="Convidados/Eventuais"/>

                  <AccordionDefault title="Relat.Habitualidade"/>
                  
                  <Accordion>

                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel2a-content"
                        id="panel2a-header"
                      >
                        <Typography component={'span'} variant={'body2'}>Documentos</Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                          <Typography component={'span'} variant={'body2'}>
                              
                              <AccordionDefault title="Pendentes - Geral"/>

                              <AccordionDefault title="Pendente - Acervo"/>

                              <AccordionDefault title="Mapa"/>
                                
                          </Typography>
                      </AccordionDetails>
                  </Accordion>

                <AccordionDefault title="Relatorio de Acervo"/>
                
                <AccordionDefault title="Carteirinhas"/>

                <AccordionDefault title="Créditos"/>

                <AccordionDefault title="Mensalidade - Anuidade"/>

                <AccordionDefault title="Profissões"/>

                <AccordionDefault title="Marcas de Armas"/>

                <AccordionDefault title="Instrutores"/>


              </AccordionDetails>
          </Accordion>

          <AccordionDefault title="Habitualidades"/>
          <AccordionDefault title="Federações"/>
          <AccordionDefault title="Treinos"/>
          <AccordionDefault title="Usuárops Sistemas"/>
          <AccordionDefault title="Fornecedores/Clientes"/>

        </AccordionDetails>
    </Accordion>
  )
}
