import styled from 'styled-components'

export const ContainerSidebar = styled.div`

  width: 20rem;

 
  transition: .5s ease;

  @media (max-width:990px){
    margin-left: -20rem;
  }
  

  .MuiSlider-root {
    color: #20b2aa;
  }
  
  .MuiButtonBase-root{
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.226);
  }
  
  .MuiButtonBase-root:hover{
    background: #c1c1c19a !important;
  }
  
  .MuiButtonBase-root:focus-within {
    background: #c1c1c19a !important;
  }
  
  .MuiButtonBase-root svg{
    margin-right: 1rem;
  }
  
  .css-15v22id-MuiAccordionDetails-root  {
    padding: 0 !important;
    padding-left: 16px !important;
  }

 

`

