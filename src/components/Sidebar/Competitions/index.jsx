import * as React from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { AccordionDefault } from '../../AccordioDefault';
import {FiTarget} from "react-icons/fi"
import {IoIosSettings} from 'react-icons/io'
import {FaTrophy} from "react-icons/fa"
import {SiTarget} from "react-icons/si"


export function Competitions(){
  return(
    <Accordion>

    <AccordionSummary
      expandIcon={<ExpandMoreIcon />}
      aria-controls="panel2a-content"
      id="panel2a-header"
    >
      <Typography component={'span'} variant={'body2'}>
        <FiTarget color="#c41d23"/>
        Competições
      </Typography>
    </AccordionSummary>
    
    <AccordionDetails>
        <Typography component={'span'} variant={'body2'}>
        
        <AccordionDefault title="Campeonatos" icon={<FaTrophy/>} path="/campeonatos" />

        <AccordionDefault title="Etapas" icon={<SiTarget/>} path="/etapas"/>

        <AccordionDefault title="Modalidades Gerais" icon={<IoIosSettings/>} path="/modalidades-gerais"/>

        <AccordionDefault title="Range Officers"/>
        
        <AccordionDefault title="Recálculos"/>
          
        </Typography>
    </AccordionDetails>
</Accordion>
  )
}