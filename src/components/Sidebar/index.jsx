import * as React from 'react'

import { AccordionDefault } from '../AccordioDefault'
import { Register } from '../Sidebar/Register'
import { Secretary } from '../Sidebar/Secretary'
import { Competitions } from './Competitions'
import { Financier } from './Financier'
import { Store } from './Store'
import { Email } from '../Sidebar/Email'
import { Access } from '../Sidebar/Access'
import { ShootingHouse } from '../Sidebar/ShootingHouse'

import { FaHome, FaArrowsAltH } from 'react-icons/fa'
import { FcGlobe, FcSettings } from 'react-icons/fc'
import { GrTasks } from 'react-icons/gr'
import { BsFillQuestionCircleFill } from 'react-icons/bs'

import { ContainerSidebar } from './styles.js'

export function Sidebar() {
  return (
    <ContainerSidebar>
      <AccordionDefault
        path="/"
        title="Home"
        icon={<FaHome color="#7ea69e" />}
      />

      <Register />

      <Secretary />

      <Competitions />

      <Financier />

      <Store />

      <AccordionDefault title="Site" icon={<FcGlobe />} />

      <Email />

      <AccordionDefault title="Configurações" icon={<FcSettings />} />

      <Access />

      <ShootingHouse />

      <AccordionDefault
        title="Tarefas Agendadas"
        icon={<GrTasks color="#141413" />}
      />

      <AccordionDefault
        title="Tutoriais"
        icon={<BsFillQuestionCircleFill color="#8ccc81" />}
      />

      <AccordionDefault
        title="Esconder Menu"
        icon={<FaArrowsAltH />}
        color="#141413"
      />
    </ContainerSidebar>
  )
}
