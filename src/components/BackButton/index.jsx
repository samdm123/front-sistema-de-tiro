import { BackButton } from "./styles"

export default function BackBtn() {
    return (
        <BackButton type="submit">
            Voltar
        </BackButton>
    )
}