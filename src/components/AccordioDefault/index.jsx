import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import Typography from '@mui/material/Typography'

export function AccordionDefault({ title, icon, path }) {

  const directToSpecificRoute = (route) =>{
   window.location.href = route === undefined ? '/' : route
  }

  return (
    <Accordion
      disableGutters={true}
      onClick={() => directToSpecificRoute(path)}
    >
      <AccordionSummary 
          aria-controls="panel1a-content" 
          id="panel1a-header"
      >
        <Typography component={'span'} variant={'body2'}>
          {' '}
          {icon}
          {title}
        </Typography>
      </AccordionSummary>
    </Accordion>
  )
}
