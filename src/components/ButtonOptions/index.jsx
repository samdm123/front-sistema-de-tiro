
export default function ButtonOptions(props){
    return(
        <>
            <div className="buttons-nav">
                <a href={props.url} className={props.classColor}>
                    {props.icon}
                </a>
                <a href={props.url} className="buttons-nav-txt">
                    {props.title}
                </a>
            </div>

        </>
    )
}