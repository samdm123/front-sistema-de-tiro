
import {createGlobalStyle} from 'styled-components'

export const Global = createGlobalStyle`

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Open Sans', sans-serif;
    }

    h1,h2,h3,h4,h5,h6,strong {
        font-weight:500;
    }


    button {
        cursor: pointer;
        border: none;
    }


    
    a{
        display:inline-block;
        text-decoration: none;
        color: #000;
    }

    
    .header-modal {
        width: 75vw;
        padding: .6rem;
        margin: .5rem 1.5rem 0 1.5rem;
        color: white;
        background: #65B951;
    }

    .form-modal {
        padding: 1rem .6rem;
        margin: .1rem 1.5rem 0 1.5rem;
        background: #FBFBFB;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
    }

    .form-modal > div {
        display: flex;
        flex-direction: column;
        gap: .5rem;
    }

    .form-modal > div > input {
        opacity: .6;
        height: 2rem;
    }

    .buttons-modal {
        width: 75vw;
        background: white;
        padding: 1rem 2rem;
        margin: 1rem 1.5rem;
        border-radius: .5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
    }

    .buttons-modal .save {
        margin-top: 0;
    }

    .buttons-modal > button {
        border: 0;
        padding: .6rem 1.3rem;
        color: white;
        cursor: pointer;
        text-transform: uppercase;
        transition: .4s;
    }

    .buttons-modal > button:hover {
        filter: brightness(110%);
    }

    .form-tag {
        display: grid;
        width: 75vw;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        gap: .6rem;
        margin-top: .1rem;
        margin-left: 2rem;
        border-radius: 0;
        label {
            display: block;
            margin-bottom: .5rem;
        }
        input, select {
            height: 2rem;
            opacity:.6;
        }
    }

    .buttons-tags {
        width: 75vw;
        display: flex;
        gap: 1rem;
        background: white;
        padding: 1rem 2rem;
        margin: 1rem 1.5rem;
        border-radius: .5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
    }

    .header-modal-tags {
        width: 75vw;
        padding: .6rem;
        margin: .5rem 1.5rem 1.5rem 1.5rem;
        color: white;
        background: #65B951;
    }

    
.pages-header + div {
    display: flex;
}

.table-filtered {
    background: white;
    margin: 1.5rem;
    width: 75vw;
    border-radius: .5rem;
    font-size: .7rem;
    text-align: left;
    padding: 1rem;
    box-shadow: 0 0 .4em rgba(0, 0, 0, 0.363);

}

.head-table > tr > th {
    width: 100em;
    border-bottom: 1px solid rgba(0, 0, 0, 0.315);
    padding: .5rem;
    color: #65B951;
}

.body-table > tr > td {
    padding: .5rem;
}

.body-table > tr > td, .body-table > tr > .buttons-table{
    border-bottom: 1px solid rgba(0, 0, 0, 0.315);
}

.buttons-table td {
    padding: .5rem;
}

.buttons-table td button {
    display: flex;
    align-items: center;
    cursor: pointer;
    font-size: .8rem;
    color: white;
    padding: .5rem;
    background: #65B951;
    border: 0;
    border-radius: .5rem;
    position: end;

    transition: .4s;
}

.buttons-table .button-table-yellow {
    background: #F6C12A;
}

.buttons-table .blue-pencel {
    background: #4285F4;
}

.buttons-table .red-trash {
    background: #E74B37;
}

.buttons-table td button:hover {
    filter: brightness(110%);
}



.pages-header {
    padding: .6rem;
    background: white;

}

.pages-header p {
    /* margin-left: 17rem; */
    color: #737373;   
    font-size: 1.3rem;
    font-weight: 300;
}

input[type=date], input[type=time]{
    width: 100%;
}



/* BOTAO SALVAR E VOLTAR */

.save {
    background: #65B951;
    margin-right: 1rem;
    margin-top: 3rem;
}

.back {
    background: #F6C12A;

}

.buttons-validate {
        width: 75vw;
        background: white;
        display: flex;
        gap: 1rem;
        padding: 1rem 2rem;
        margin: 1.5rem;
        border-radius: .5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
}



@media (max-width: 990px) {
    .table-filtered {
        background: white;
        margin: 1.5rem;
        width: 75vw;
        border-radius: .5rem;
        font-size: .7rem;
        text-align: left;
        padding: 1rem;
        box-shadow: 0 0 .4em rgba(0, 0, 0, 0.363);
        display: block;
        overflow-x: auto;    
    }

    .pages-header {
        width: 100vw;
    }

    .pages-header p {
        margin-left: 0rem;
        width: 100%;

    }
}


`