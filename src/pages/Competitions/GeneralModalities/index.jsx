import { useState } from 'react';
import Modal from 'react-modal'

import { BsFillPencilFill, BsFillTrashFill } from 'react-icons/bs';


import {ContainerGeneralModalities} from './styles.js'

Modal.setAppElement('#root')

export default function GeneralModalities() {
    const [modalIsOpen, setIsOpen] = useState(false)

    function handleOpenModal() {
        setIsOpen(true)
    }

    function handleCloseModal() {
        setIsOpen(false)
    }

    const customStyles = {
        content: {
            boxShadow: '0 0 .4em rgba(0, 0, 0, 0.363)',
            display: 'flex',
            justifyContent: 'center',
        }
    }  


    return ( 
        <ContainerGeneralModalities>
            <header className='pages-header'>
                <p>Modalidades Gerais</p>
            </header>
            <div>
                <main className='general-modalities-main'>
                    <div className="div-add-generalModalities">
                        <a href="#" className='add-generalModalities' onClick={handleOpenModal}>+ ADICIONAR MODALIDADE GERAL</a>
                    </div>
                    <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal} style={customStyles}>
                        <div className="modal-content">
                            <div className="header-modal">
                                <p>Adicionar Modalidade Geral</p>
                            </div>
                            <form action="" className='form-modal'>
                                <div>
                                    <label htmlFor="modality-name-form">Nome da Modalidade:</label>
                                    <input type="text" name="modality-name-form" id="modality-name-form" placeholder='Ex: Trap Americano' />
                                </div>
                            </form>

                            <div className="buttons-modal">
                                <button className='save' type="submit">Salvar</button>
                                <button className='back' onClick={handleCloseModal}>Voltar</button>
                            </div>
                        </div>
                    </Modal>
                    <div className="table-filtered">
                        <ul>
                            <thead className='head-table'>
                                <tr>
                                    <th>
                                        MODALIDADE
                                    </th>
                                </tr>
                            </thead>
                            <tbody className='body-table'>
                                <tr>
                                    <td>
                                        ARCO E FLECHA
                                    </td>
                                    <div className="buttons-table">
                                        <td><button className='blue-pencel'><BsFillPencilFill /></button></td>
                                        <td><button className='red-trash'><BsFillTrashFill /></button></td>
                                    </div>
                                </tr>
                                <tr>
                                    <td>
                                        CARABINA DE AR MIRA ABERTA
                                    </td>
                                    <div className="buttons-table">
                                        <td><button className='blue-pencel'><BsFillPencilFill /></button></td>
                                        <td><button className='red-trash'><BsFillTrashFill /></button></td>
                                    </div>
                                </tr>
                                <tr>
                                    <td>
                                        CARABINA ESPORTE
                                    </td>
                                    <div className="buttons-table">
                                        <td><button className='blue-pencel'><BsFillPencilFill /></button></td>
                                        <td><button className='red-trash'><BsFillTrashFill /></button></td>
                                    </div>
                                </tr>
                            </tbody>
                        </ul>
                    </div>
                    
                </main>
            </div>
        </ContainerGeneralModalities>
     );
}