import styled from 'styled-components'

export const ContainerGeneralModalities = styled.section`
    background-color: #eee;

    display: flex;
    flex-direction: column;
    width:100%;
    
    .pages-header{
        padding: .5rem;
    }

 

    .general-modalities-main .div-add-generalModalities  {
        display: flex;
        
        align-items: center;
        /* margin-top: 1.5rem;
        margin-left: 2rem; */
        justify-content: flex-end;
        /* width: auto; */
        padding: .5rem;
        text-decoration: none;
        color: white;
        /* background: #65B951; */
        font-size: .8rem;

        transition: .4s;

        .add-generalModalities{
            padding: .5rem;
            background-color: #65b951;
            border-radius: .25rem;
            color: white;
            box-shadow: 3px 5px 8px rgba(0,0,0,0.2);
        }
    } 

    .general-modalities-main .add-generalModalities:hover {
        filter: brightness(110%);
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;

        header {
            p {
                margin-left: 2rem;
            }
        }
    }

`


