import styled from 'styled-components'

export const ContainerAppBackup = styled.section`

    background-color: #eee;

    .header-table-backup {
        width: 75vw;
        padding: .6rem;
        margin: 1.5rem 1.5rem 0 1.5rem;
        color: white;
        background: #11A9CC;
    }

    #file-backup {
        border: 1px solid rgba(0, 0, 0, 0.301);
        width: 20rem;
    }

    .form-backup {
        margin: 0 1.5rem;
        border-radius: 0;
        background-color: white;
        padding: 1rem;
        label {
            display: block;
        }
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;

        header {
            p {
                margin-left: 2rem;
            }
        }
    }

    @media (max-width: 750px) {
        #file-backup {
            width: 15rem;
        }
    }



`

