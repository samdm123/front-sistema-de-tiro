import BackButton from '../../../../components/BackButton'
import SaveButton from '../../../../components/SaveButton'

// import './styles.js'

import {ContainerAppBackup} from './styles'

export default function AppBackup() {
    return (
        <ContainerAppBackup>
            <header className='pages-header'><p>Backup do aplicativo ( ShootingScore)</p></header>
            <div>
                <main className='main-appBackup'>
                    <div className="header-table-backup">
                        <p>Backup do aplicativo</p>
                    </div>
                    <form action="" className='form-backup'>
                        <div>
                            <label htmlFor="file-backup">Arquivo</label>
                            <input type="file" name="file-backup" id="file-backup" />
                        </div>
                    </form>
                    <div className="buttons-validate">
                        <SaveButton />
                        <BackButton />
                    </div>
                </main>
            </div>
        </ContainerAppBackup>
    )
}