import {ContainerReportPhases} from "./styles.js"
import './styles.js'

export default function ReportPhases() {
    return (
        <ContainerReportPhases>
            <header className='pages-header'><p>Relatório das Etapas</p></header>

             <div>
                 {/* <Sidebar /> */}
                 <main className='main-reportPhases'>
                    <div className="header-table-backup">
                        <p>Relação de Etapas - Temporada</p>
                    </div>

                    <div className="table-filtered report-phases">
                        <ul>
                            <thead className='head-table'>
                                <tr>
                                    <th>ETAPA</th>
                                    <th>CAMPEONATO</th>
                                    <th>DATA</th>
                                    <th>INSCRITOS</th>
                                </tr>
                            </thead>

                            {/* <tbody className='body-table'>
                                <tr>
                                    <td>1ª ETAPA</td>
                                    <td>CAMPEONATO DE TIRO - ICT</td>
                                    <td>18/02/2021</td>
                                    <td>18</td>
                                </tr>
                            </tbody> */}
                            
                        </ul>
                    </div>
                 </main>
             </div>

        </ContainerReportPhases>
    )
}