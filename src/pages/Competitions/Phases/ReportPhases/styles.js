import styled from 'styled-components'

export const ContainerReportPhases = styled.section`
    background-color: #eee;

    .header-table-backup {
        margin: 1rem 1.5rem;
    }

    .table-filtered.report-phases {
        margin-top: 1rem;
        border-radius: 0;
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;

        header {
            p {
                margin-left: 2rem;
            }
        }

        .header-table-backup {
            width: 75vw;
        }
    }

`



