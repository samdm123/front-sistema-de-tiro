import styled from 'styled-components'

export const ContainerSettingsCompetitions = styled.section`

    background-color: #eee;

    .validate-subscriptions {
        width: 75vw;
        background: white;
        padding: 2rem;
        margin: 1.5rem;
        border-radius: .5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
    }


    .validate-subscriptions p {
        margin-bottom: 1rem;
    }

    .checks {
        display: flex;
        align-items: center;
        gap: .6rem;

        margin-top: .5rem;
    }

    .checks label {
        cursor: pointer;
    }

    input[type=checkbox] {
        transform: scale(1.5);
        opacity: .6;
        cursor: pointer;
    }

    .buttons-validate {
        width: 75vw;
        background: white;
        padding: 1rem 2rem;
        margin: 1.5rem;
        border-radius: .5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
    }

    .buttons-validate .save {
        margin-top: 0;
    }

    .buttons-validate > button {
        border: 0;
        padding: .6rem 1.3rem;
        color: white;
        cursor: pointer;
        text-transform: uppercase;
        transition: .4s;
    }

    .buttons-validate > button:hover {
        filter: brightness(110%);
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;

        header {
            p {
                margin-left: 2rem;
            }
        }
    }

`