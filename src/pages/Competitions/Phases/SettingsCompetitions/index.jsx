import { ContainerSettingsCompetitions } from './styles'

import { checkItems } from '../../../../services/database'

export default function SettingsCompetitions() {
  return (
    <ContainerSettingsCompetitions>
      <header className="pages-header">
        <p>Configurações - Competições</p>
      </header>
      <div>
        <main className="main-settingsCompetitions">
          <div className="validate-subscriptions">
            <p>Validar Inscrições:</p>

            {checkItems.map((item, index) => (
              <div key={index} className="checks">
                <input
                  type="checkbox"
                  name={item.forNameId}
                  id={item.forNameId}
                />
                <label htmlFor={item.forNameId}>{item.label}</label>
              </div>
            ))}
          </div>

          <div className="buttons-validate">
            <button className="save" type="submit">
              Salvar
            </button>
            <button className="back">Voltar</button>
          </div>
        </main>
      </div>
    </ContainerSettingsCompetitions>
  )
}
