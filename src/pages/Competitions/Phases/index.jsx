// import { Sidebar } from '../../components/Sidebar'
import { IoSettings } from 'react-icons/io5'
import { BsCheckLg, BsTagsFill } from 'react-icons/bs'
import { FaRegCheckCircle, FaUserAlt } from 'react-icons/fa'
import { AiFillStar, AiFillWarning } from 'react-icons/ai'
import { ContainerPhases } from './styles'

import { IoIosArrowDown } from 'react-icons/io'

export default function Phases() {

  
  return (
    <ContainerPhases>
      <header className="pages-header">
        <p>Etapas</p>
      </header>
      <div>
        <main className="main-phase">
          <div className="buttons-top">
            <a
              href="/etapas/configuracoes/competicao"
              className="config-phase button blue"
            >
              <IoSettings />
              CONFIGURAR
            </a>
            <a
              href="/etapas/etiquetas"
              className="hang-tags button purple"
            >
              <BsTagsFill />
              ETIQUETAS
            </a>
            <a
              href="/etapas/backup-do-aplicativo"
              className="backup-app button yellow"
            >
              <IoSettings />
              BACKUP DO APLICATIVO
            </a>
            <a
              href="/etapas/relatorio-de-etapas"
              className="report-phases button blue-light"
            >
              <FaRegCheckCircle />
              RELATÓRIO DAS ETAPAS
            </a>
            <a
              href="/etapas/relatorio-do-atleta"
              className="report-athlete button red"
            >
              <FaUserAlt />
              RELATÓRIO DO ATLETA
            </a>
            <a
              href="/etapas/adicionar-etapa"
              className="add-phase button green-dark"
            >
              + ADICIONAR ETAPA
            </a>
          </div>

          <form action="" className="form-phase">
            <div className="divform">
              <div>
                <label htmlFor="phase">Etapa</label>
                <input type="text" id="phase" placeholder="Nome da Etapa" />
              </div>

              <div>
                <label htmlFor="season">Temporada</label>
                <select name="season" id="season">
                  <option value="1">Todas</option>
                  <option value="2">2023</option>
                  <option value="3">2022</option>
                  <option value="4">2021</option>
                  <option value="5">2020</option>
                  <option value="6">2019</option>
                  <option value="7">2018</option>
                  <option value="8">2017</option>
                  <option value="9">2016</option>
                  <option value="10">2015</option>
                  <option value="11">2014</option>
                  <option value="12">2013</option>
                  <option value="13">2012</option>
                  <option value="14">2011</option>
                  <option value="15">2010</option>
                  <option value="16">2009</option>
                </select>
              </div>

              <div>
                <label htmlFor="champ-phase">Campeonato</label>
                <select name="champ-phase" id="champ-phase">
                  <option value="0">Todos</option>
                </select>
              </div>

              <div className="phase-date-fields">
                <div>
                  <label htmlFor="phase-date">Data de</label>
                  <input
                    type="date"
                    id="phase-date-init"
                    className="phase-date"
                  />
                </div>

                <div>
                  <label htmlFor="phase-date">Data até</label>
                  <input
                    type="date"
                    id="phase-date-final"
                    className="phase-date"
                  />
                </div>
              </div>

              <div>
                <label htmlFor="phase-deleted">Etapa Deletada</label>
                <select name="phase-deleted" id="phase-deleted">
                  <option value="0">Não</option>
                  <option value="1">Sim</option>
                </select>
              </div>

              <div className="buttons-bot-phase">
                <button type="reset" className="clear-filter">
                  Limpar filtro
                </button>
                <button type="submit" className="filter-btn">
                  Filtrar
                </button>
              </div>
            </div>
          </form>

          <div className="table-filtered">
            <ul>
              <thead className="head-table">
                <tr>
                  <th>ETAPA</th>
                  <th>CAMPEONATO</th>
                  <th>TEMPORADA</th>
                  <th>N°ETAPA</th>
                  <th>DATA</th>
                </tr>
              </thead>

              <tbody className="body-table">
                <tr>
                  <td>GRANDE INAUGURAÇÃO</td>
                  <td>CAMPEONATO DE TIRO - ICT</td>
                  <td>2021</td>
                  <td>1ª ETAPA</td>
                  <td>18/02/2021</td>
                  <div className="buttons-table">
                    <td>
                      <button>
                        <BsCheckLg />
                      </button>
                    </td>
                    <td>
                      <button>
                        <AiFillStar />
                      </button>
                    </td>
                    <td>
                      <button className="button-table-yellow">
                        <IoIosArrowDown />
                      </button>
                    </td>
                  </div>
                </tr>
              </tbody>
            </ul>
          </div>

          <div className="warning disable">
            <AiFillWarning /> Você não possui nenhum dado cadastrado.
          </div>
        </main>
      </div>
    </ContainerPhases>
  )
}
