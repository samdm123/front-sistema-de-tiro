// import { Sidebar } from '../../../components/Sidebar'

import './styles.js'

import {ContainerReportAthlete} from './styles'

export default function ReportAthlete() {
    return (
        <ContainerReportAthlete>
            <header className='pages-header'><p>Habitualidade</p></header>

            <div className='reportAthlete-main'>
                <main className='main-reportAthlete'>
                    <form action="" className='form-reportAthlete'>
                        <div>
                            <label htmlFor="search-athlete">Atleta:</label>
                            <input type="search" name="search-athlete" id="search-athlete" />
                        </div>
                        <div>
                            <label htmlFor="time-of">Período de:</label>
                            <input type="date" />
                        </div>
                        <div>
                            <label htmlFor="time-to">Período até:</label>
                            <input type="date" />
                        </div>
                        <div>
                            <label htmlFor="CR">CR</label>
                            <select name="CR" id="CR">
                                <option value="0">Todos</option>
                                <option value="1">Com CR cadastrado</option>
                                <option value="2">Sem CR cadastrado</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="modalities">Modalidades:</label>
                            <input type="text" />
                        </div>
                        <div>
                            <label htmlFor="weapon">Arma:</label>
                            <select name="weapon" id="weapon">
                                <option value="0">Todas</option>
                                <option value="1">Canhão</option>
                                <option value="2">Carabina</option>
                                <option value="3">Espingarda</option>
                                <option value="4">Fuzil</option>
                                <option value="5">Garrucha</option>
                                <option value="6">Pistola</option>
                                <option value="7">Revolver</option>
                                <option value="8">Rifle</option>
                                <option value="9">Submetralhadora</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="caliber">Calibre:</label>
                            <input type="text" id='caliber'/>
                        </div>
                    </form>

                    <div className="buttons-tags">
                        <button type="submit">Buscar</button>
                        <button>Imprimir</button>
                    </div>

                    <div className="athlete">
                        <div className="info-athlete">
                            <img src="" alt="" />
                            <h3>A RODOVIA AM-010, RAMAL DO AIBU, KM 05, S/N, KM 252, AREA RURAL DE ITACOATIARA, CEP: 69109-899, Itacoatiara-AM</h3>
                            <h4>CNPJ: 35.795.294/0001-70</h4>
                            <h4>itacoatiaraclubedetiro.com.br</h4>
                        </div>
                        <div className="report-athlete">
                            <p>RELATÓRIO DO ATLETA</p>
                            <p className='info-register-athlete'><b>ATLETA:</b> ÍCARO VINÍCIUS DE SOUZA NASCIMENTO | MAT: 2 | CR: 176142</p>
                            <ul className='list-info-athlete'>
                                <tr>
                                    <th>NR</th>
                                    <th>DATA</th>
                                    <th>EVENTO</th>
                                    <th>MODALIDADE</th>
                                    <th>LOCAL</th>
                                    <th>TIPO ARMA</th>
                                    <th>CALIBRE</th>
                                    <th>MUNIÇÃO</th>
                                    <th>NÍVEL</th>
                                </tr>
                            </ul>
                        </div>
                    </div>
                </main>
            </div>
        </ContainerReportAthlete>
    )
}