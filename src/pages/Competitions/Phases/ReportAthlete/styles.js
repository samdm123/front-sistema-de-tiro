import styled from 'styled-components'

export const ContainerReportAthlete = styled.section`
    background-color: #eee;

    label {
        display: block;
    }

    input, select {
        opacity: .6;
        height: 2rem;
    }

    .form-reportAthlete {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        grid-gap: 1rem;
        margin: 1.5rem;
        background-color: white;
        padding: 1rem;
        border-radius: .5rem;
    }

    .info-athlete {
        text-align: center;
        font-size: .8rem;
        line-height: 2;
        margin-bottom: 2rem;
    }

    .report-athlete p:first-child {
        text-align: center;
        font-size: 2rem;
        font-weight: 300;
        margin-bottom: 1rem;
    }

    .info-register-athlete {
        margin: 1.5rem;
    }

    .list-info-athlete {
        margin: 1.5rem;
    }

    .list-info-athlete > tr > th {
        width: 8rem;
        background-color: lightgray;
        font-size: .9rem;
        font-weight: 400;
    }

    .buttons-tags button {
        width: 8rem;
        height: 2rem;
        color: white;
        transition: .4s;
    }

    .buttons-tags button:hover {
        filter: brightness(110%);
    }

    .buttons-tags button:first-child {
        background-color: #4285F4;
    }

    .buttons-tags button:last-child {
        background-color: #65B951;
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;

        header {
            p {

                margin-left: 5rem;
            }
        }

        .form-reportAthlete {
            margin-left: 10rem;
        }

        .info-athlete, .report-athlete {
            margin-left: 8rem;

        }

        .buttons-tags {
            margin-left: 10rem;
        }

        .info-athlete {
            margin-left: 8rem;
        }

        .list-info-athlete {
            display: block;
            overflow-x: auto;
            margin-left: 10rem;
        }

        .list-info-athlete > tr > th {
            width: 10rem;
            margin: 2rem;
            background-color: lightgray;
            font-size: .9rem;
            font-weight: 400;
            display: block;
        }

        .form-reportAthlete input, .form-reportAthlete select, .form-reportAthlete input[type=date] {
            width: 100%;
        }
    }

`
