import styled from 'styled-components'


export const ContainerAddPhase = styled.section`

    background-color: #eee;

    input, select {
        opacity: .6;
        height: 2rem;
    }

    label {
            display: block;
            margin-bottom: .5rem;
    }

    .header-form-addPhase {
        width: 75vw;
        padding: .6rem;
        margin: 1.5rem 1.5rem 0 1.5rem;
        color: white;
        background: #11A9CC;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .header-form-addPhase a {
        text-decoration: none;
        color: white;
        font-weight: 700;
        background: #65B951;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .form-addPhase {
        margin-top: 0;
        border-radius: 0;
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        gap: .6rem;
        margin-left: 1.5rem;
        background-color: white;
        padding: 1rem;
        width: 75vw;
    }

    .form-addPhase >  div > input {
        opacity: .4;
    }

    .form-addPhase >  div > select {
        opacity: .4;
    }

    .form-addPhase-modalitie {
        display: block;
        margin-top: 0;
        border-radius: 0;
        margin-left: 1.5rem;
        background-color: white;
        padding: 1rem;
        width: 75vw;

    }

    .values-for-modalities {
        margin-top: -.4rem;
        border-radius: 0;
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        gap: .6rem;
        margin-bottom: 2rem;
    }

    .plus.nothing {
        background: none;
    }

    .deleteXdiv {
        display: flex;
        justify-content: center;
        margin-top: 1.8rem;
        width: 2rem;
        height: 2rem;
        background: #be2931;
    }

    .excluirX {
        text-decoration: none;
        color: white;
        font-weight: 700;
        font-size: 1.3rem;
       

        transition: .4s;
    }

    .excluirX:hover {
        filter: brightness(110%);
    }

    .bottom-bar {
        width: 70vw;
        height: .1rem;
        background-color: rgba(0, 0, 0, 0.164);
        display: block;
        margin-left: .7rem;
        margin-bottom: 2rem;
    }

    .buttons-validate {
        display: flex;
        gap: 1rem;
        margin: 2rem 0 2rem 1.5rem;
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        gap: 1rem;

        header {
            p {
                margin-left: 2rem;
            }
        }

        .form-addPhase >  div > input {
            opacity: .4;
            width: 100%;
        }
        
        .form-addPhase >  div > select {
            opacity: .4;
            width: 100%;
        }

        .values-for-modalities > div > input, .values-for-modalities > div > select{
            width: 100%;
        }

        .bottom-bar {
            margin-left: 0;
            width: 100%;
        }
    }
`

