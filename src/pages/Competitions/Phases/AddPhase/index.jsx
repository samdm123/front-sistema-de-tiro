
import { itemsAddPhase } from '../../../../services/database'
import SaveButton from '../../../../components/SaveButton'
import BackButton from '../../../../components/BackButton'

import {ContainerAddPhase} from './styles'

export default function AddPhase() {
    let phaseOptions = []
    for (let i = 1; i < 100; i++) {
        phaseOptions.push(<option value={i}>{i}° Etapa</option>)
    }
    return (
        <ContainerAddPhase>
            <header className='pages-header'><p>Adicionar Etapa</p></header>
            <div>
                <main className='addPhase-main'>
                    <div className="header-form-addPhase">
                        <p>Etapa</p>
                    </div>
                    <form action="" className='form-addPhase'>
                        <div>
                            <label htmlFor="title-phase">Título</label>
                            <input type="text" id='title-phase' placeholder='Ex: Liga Nacional de Trap Americano'/>
                        </div>
                        <div>
                            <label htmlFor="title-phase">Temporada</label>
                            <select name="season" id="season">
                                <option value="1">Todas</option>
                                <option value="2">2023</option>
                                <option value="3">2022</option>
                                <option value="4">2021</option>
                                <option value="5">2020</option>
                                <option value="6">2019</option>
                                <option value="7">2018</option>
                                <option value="8">2017</option>
                                <option value="9">2016</option>
                                <option value="10">2015</option>
                                <option value="11">2014</option>
                                <option value="12">2013</option>
                                <option value="13">2012</option>
                                <option value="14">2011</option>
                                <option value="15">2010</option>
                                <option value="16">2009</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="champ">Campeonato</label>
                            <select name="champ" id="champ">
                                <option value="0">Todos</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="phase-number">N° Etapa:</label>
                            <select name="phase-number" id="phase-number">
                                <option value="0">Selecione...</option>
                                {phaseOptions}
                            </select>
                        </div>
                        <div>
                            <label htmlFor="phase-wheight">Peso da Etapa:</label>
                            <select name="phase-wheight" id="phase-wheight">
                                <option value="0">Selecione...</option>
                                <option value="1">x1</option>
                                <option value="2">x2</option>
                                <option value="3">x3</option>
                                <option value="4">x4</option>
                                <option value="4">x5</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="level">Nível:</label>
                            <select name="level" id="level">
                                <option value="0">Selecione...</option>
                                <option value="1">MUNICIPAL</option>
                                <option value="2">ESTADUAL</option>
                                <option value="3">REGIONAL</option>
                                <option value="4">NACIONAL</option>
                                <option value="4">INTERNACIONAL</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="date-init">Data de início</label>
                            <input type="date" id='date-init'/>
                        </div>
                        <div>
                            <label htmlFor="date-final">Data de Encerramento</label>
                            <input type="date" id='date-final'/>
                        </div>
                        <div>
                            <label htmlFor="date-final-sub">Data de Encerramento Inscrições</label>
                            <input type="date" id='date-final-sub'/>
                        </div>
                        <div>
                            <label htmlFor="hour-final-sub">Data de Encerramento Inscrições</label>
                            <input type="time" id='hour-final-sub'/>
                        </div>
                        <div>
                            <label htmlFor="sub-for-site">Inscrições via Site</label>
                            <select name="sub-for-site" id="sub-for-site">
                                <option value="0">Somente até início da etapa</option>
                                <option value="1">Antes e durante a etapa</option>
                            </select>
                        </div>

                        {itemsAddPhase.map( (item, index) => (
                            <div key={index}>
                                <label htmlFor={item.forNameId}>{item.label}</label>
                                <select name={item.forNameId} id={item.forNameId}>
                                    <option value="0">{item.op01}</option>
                                    <option value="1">{item.op02}</option>
                                </select>
                            </div>
                        ))}

                        <div>
                            <label htmlFor="ticket-validate">Validade Boleto</label>
                            <select name="ticket-validate" id="ticket-validate">
                                <option value="0">No último dia das inscrições</option>
                                <option value="1">1 dia após ter realizado a inscrição</option>
                                <option value="2">2 dias após ter realizado a inscrição</option>
                                <option value="3">3 dias após ter realizado a inscrição</option>
                                <option value="4">4 dias após ter realizado a inscrição</option>
                                <option value="5">5 dias após ter realizado a inscrição</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="squad-value">Valor Equipe:</label>
                            <input type="text" name="squad-value" id="squad-value" />
                        </div>
                        <div>
                            <label htmlFor="credit">Crédito</label>
                            <select name="credit" id="credit">
                                <option value="0">Selecione...</option>
                                <option value="1">Inscrições e itens adicionais</option>
                                <option value="2">Somente em inscrições</option>
                                <option value="3">Somente em itens adicionais</option>
                                <option value="4">Não usar crédito</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="track-assembly">Montagem de Pista</label>
                            <select name="track-assembly" id="track-assembly">
                                <option value="0">Desativar</option>
                                <option value="1">Ativar</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="squad">Esquadra:</label>
                            <select name="squad" id="squad">
                                <option value="0">Desativar</option>
                                <option value="1">Ativar</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="integration-account">Conta Integração</label>
                            <select name="integration-account" id="integration-account">
                                <option value="0">Selecione uma conta...</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="surcharge">Taxa Extra:</label>
                            <input type="text" name="surcharge" id="surcharge" />
                        </div>
                        <div>
                            <label htmlFor="surcharge-description">Desc. Taxa Extra:</label>
                            <input type="text" name="surcharge-description" id="surcharge-description" placeholder='Ex: Taxa Organização'/>
                        </div>
                        <div>
                            <label htmlFor="link-sub">Link de inscrição externo:</label>
                            <input type="text" name="link-sub" id="link-sub" placeholder='Ex: www.clube.com/inscrição'/>
                        </div>
                        <div>
                            <label htmlFor="message-final-sub">Mensagem ao atleta quando inscrição encerrou:</label>
                            <input type="text" name="message-final-sub" id="message-final-sub" placeholder='Padrão: inscrição encerrada'/>
                        </div>
                        <div>
                            <label htmlFor="modalities-for-athlete">Modalidades por Atleta:</label>
                            <select name="modalities-for-athlete" id="modalities-for-athlete">
                                <option value="0">Todas</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="result-of">Resultado de:</label>
                            <input type="date" id='result-of'/>
                        </div>
                        <div>
                            <label htmlFor="result-to">Resultado até:</label>
                            <input type="date" id='result-to'/>
                        </div>
                        <div>
                            <label htmlFor="integration-calendar">Integração Calendário</label>
                            <select name="integration-calendar" id="integration-calendar">
                                <option value="0">Sem integração calendário</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="club-apply">Clube se candidatar:</label>
                            <select name="club-apply" id="club-apply">
                                <option value="0">Não</option>
                                <option value="1">Sim, sem validação</option>
                                <option value="2">Sim, com mensalidade/anuidade em dia</option>
                            </select>
                        </div>
                        <div>
                            <label htmlFor="date-block-result">Data de Bloqueio de Resultado:</label>
                            <input type="date" id='date-block-result'/>
                        </div>
                        <div>
                            <label htmlFor="hour-block-result">Hora Bloqueio de Resultado:</label>
                            <input type="time" id='hour-block-result'/>
                        </div>
                        
                    </form>

                    <div className="header-form-addPhase">
                        <p>Valores por Nº de Modalidades</p>
                        <a href="#" className='plus nothing'>+</a>
                    </div>
                    <form action="" className='form-addPhase-modalitie'>
                        <div className="values-for-modalities">
                            <div>
                                <label htmlFor="modalitie">Modalidade:</label>
                                <select name="modalitie" id="modalitie">
                                    <option value="0">QUALQUER MODALIDADE</option>
                                </select>
                            </div>
                            <div>
                                <label htmlFor="condition">Condição:</label>
                                <select name="condition" id="condition">
                                    <option value="0">Selecione...</option>
                                    <option value="1">&gt;</option>
                                    <option value="2">&lt;</option>
                                    <option value="3">=</option>
                                </select>
                            </div>

                            <div>
                                <label htmlFor="number-modalities">Nº de Modalidades:</label>
                                <select name="number-modalities" id="number-modalities">
                                    <option value="0">Selecione...</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                </select>
                            </div>

                            <div>
                                <label htmlFor="online-value">Valor Online:</label>
                                <input type="text" id='online-value'/>
                            </div>
                            <div>
                                <label htmlFor="local-value">Valor Local:</label>
                                <input type="text" id='local-value'/>
                            </div>
                            <div>
                                <label htmlFor="online-value-visit">Valor Convidado Online:</label>
                                <input type="text" id='online-value-visit'/>
                            </div>
                            <div>
                                <label htmlFor="local-value-visit">Valor Convidado Local:</label>
                                <input type="text" id='local-value-visit'/>
                            </div>
                            <div className="deleteXdiv">
                                <a href="#" className='plus excluirX'>x</a>
                            </div>
                           
                        </div>
                        <div className="bottom-bar"></div>                  
                        
                    </form>

                    <div className="header-form-addPhase">
                        <p>Convite da Etapa</p>
                    </div>

                    <div className="header-form-addPhase">
                        <p>Termo de aceite ao realizar a inscrição</p>
                    </div>

                    <div className="buttons-validate">
                        <SaveButton />
                        <BackButton />
                    </div>
                </main>
            </div>
        </ContainerAddPhase>
    )
}