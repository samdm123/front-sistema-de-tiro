import styled from 'styled-components'

export const ContainerPhases = styled.section`
    width: 100%;
    padding: .5rem;
    background-color: #eee;

    label {
        display: block;
        margin-bottom: .5rem;
    }

    input, select {
        opacity: .6;
        height: 2rem;
    }

    .buttons-top {
        display: flex;
        align-items: center;
        gap: .2rem;
        margin-top: 1.5rem;
        margin-left: 1.8rem;
        text-decoration: none;
        font-weight: 400;
        width: 75vw;

    }

    .button {
        width: 10rem;
        text-align: center;
        color: white;
        text-decoration: none;
        padding: .5rem;
        display: flex;
        justify-content: center;
        align-items: center;
        gap: .4rem;
        transition: .4s;
        font-size: .7rem;
        color: black;
    }

    .button.blue {
        border-radius: 0 0 0 0;
        width: 8rem;
        background-color: #4285F4;
        color: white;

    }

    .button.purple {
        border-radius: 0 0 0 0;
        width: 7rem;
        background-color: #8C44A2;
        color: white;

    }

    .button.yellow {
        border-radius: 0 0 0 0;
        margin-right: 2rem;
        width: 13rem;
        background-color: #F6C12A;
        color: white;

    }

    .button.blue-light {
        border-radius: 0 0 0 0;
        width: 13rem;
        background-color: #28B3D3;
        color: white;
    }

    .button.red {
        border-radius: 0 0 0 0;
        width: 12rem;
        background-color: #E74B37;
        color: white;

    }

    .button.green-dark {
        border-radius: 0 0 0 0;
        background-color: #65B951;
        color: white;

    }

    main .button:hover {
        filter: brightness(110%);
    }

    .divform {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));
        gap: 2rem;
        width: 75vw;
        margin-left: 1.5rem;
        margin-top: 1.5rem;
        background-color: white;
        padding: 1rem;
        border-radius: .5rem;
        box-shadow: 0 0 .4em rgba(0, 0, 0, 0.363);

    }

    .phase-date-fields {
        display: flex;
        gap: 1rem;
        margin-right: 1rem;
    }

    #phase {
        width: 100%;
    }

    #champ-phase {
        width: 100%;
    }

    .phase-date {
        width: 100%;
    }

    .buttons-bot-phase {
        display: flex;
        gap: 1rem;
        align-items: center;
    }

    .clear-filter {
        color: white;
        background-color: #F6C12A;
        height: 2rem;
        width: 8rem;
        text-transform: uppercase;
    }

    .filter-btn {
        color: white;
        background-color: #65B951;
        height: 2rem;
        width: 8rem;
        text-transform: uppercase;
    }

    .warning {
        margin-top: 4rem;
        width: 64rem;
        margin-left: 1.5rem;
        padding: .8rem;
        background: #FFF1A8;
        border-radius: .5rem;
        font-weight: 300;
        display: flex;
        align-items: center;
        gap: .5rem;
    }

    .disable {
        display: none;
    }
    
    .buttons-table {
        margin-top: .4rem;
    }



    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;

        header {
            margin-left: 2rem;
        }

        .buttons-top {
            display: block;
            margin-left: .5rem;
            text-decoration: none;
            font-size: .6rem;
            font-weight: 400;
            /* width: 100%; */
            margin-left: 2rem;
        
        }

        .button {
            width: 100%;
            text-align: center;
            color: white;
            text-decoration: none;
            padding: .5rem;
            display: flex;
            justify-content: center;
            align-items: center;
            gap: .4rem;
            transition: .4s;
            font-size: .8rem;
            margin-bottom: .5rem;
        
        }
        
        .button.blue {
            border-radius: 0 0 0 0;
            width: 100%;
        }
        
        .button.purple {
            border-radius: 0 0 0 0;
            width: 100%;
        
        }
        
        .button.yellow {
            border-radius: 0 0 0 0;
            /* margin-right: 2.5rem; */
            width: 100%;
        }
        
        .button.blue-light {
            border-radius: 0 0 0 0;
            width: 100%;
        
        }
        
        .button.red {
            border-radius: 0 0 0 0;
            width: 100%;
            margin-right: 0;
        }
        
        .button.green-dark {
            border-radius: 0 0 0 0;
        
        }

        .form-phase {
            display: block;
            width: 100%;
            margin-left: .5rem;
        }

        .form-phase div input, .form-phase select, .form-phase input[type=date] {
            width: 100%;
        }

        
        .phase-date-fields {
            display: block;
            gap: 1rem;
            width: 100%;
        }

        #phase {
            width: 100%;
        }

        #champ-phase {
            width: 100%;
        }

        .phase-date {
            width: 100%;
        }

        .table-filtered {
            margin-left: 2rem;
        }
    }

    @media (max-width:400px) {
        .form-phase input, .form-phase select, .form-phase input[type=date] {
            width: 16rem;

        }

        #phase {
            width: 16rem;
        }

        #champ-phase {
            width: 16rem;

        }

        .buttons-bot-phase {
            display: flex;
            flex-direction: column;
            gap: 0rem;
        }

        .buttons-bot-phase button {
            width: 16rem;
            
        }
    }




`
