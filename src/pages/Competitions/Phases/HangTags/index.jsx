import Modal from 'react-modal'
import { useState } from 'react'
import SaveButton from '../../../../components/SaveButton'
import BackButton from '../../../../components/BackButton'

import { AiFillStar } from 'react-icons/ai'
import { BsFillPencilFill, BsFillTrashFill } from 'react-icons/bs'

import { itemsTags } from '../../../../services/database'

import { ContainerHangTags } from './styles.js'

Modal.setAppElement('#root')

export default function HangTags() {
  const [modalIsOpen, setIsOpen] = useState(false)

  function handleOpenModal() {
    setIsOpen(true)
  }

  function handleCloseModal() {
    setIsOpen(false)
  }

  const customStyles = {
    content: {
      boxShadow: '0 0 .4em rgba(0, 0, 0, 0.363)',
      display: 'flex',
      justifyContent: 'center'
    }
  }

  return (
    <ContainerHangTags>
      <header className="pages-header">
        <p>Etiquetas</p>
      </header>
      <div>
        <main className="main-hangTags">
          <div className="div-add-ht">
            <a href="#" className="add-ht" onClick={handleOpenModal}>
              + ADICIONAR ETIQUETA
            </a>
          </div>
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={handleCloseModal}
            style={customStyles}
          >
            <div className="modal-content">
              <div className="header-modal-tags">
                <p>Etiquetas</p>
              </div>
              <form action="" className="form-tag">
                <div>
                  <label htmlFor="tag-name-form">Nome da Etiqueta</label>
                  <input
                    type="text"
                    name="tag-name-form"
                    id="tag-name-form"
                    placeholder="Ex: Oficial"
                  />
                </div>
                <div>
                  <label htmlFor="tag-description-form">
                    Descrição no Topo
                  </label>
                  <input
                    type="text"
                    name="tag-description-form"
                    id="tag-description-form"
                    placeholder="Ex: Calça"
                  />
                </div>
                <div>
                  <label htmlFor="topMargin">Margem Superior</label>
                  <input
                    type="text"
                    name="topMargin"
                    id="topMargin"
                    placeholder="2,00"
                  />
                </div>
                <div>
                  <label htmlFor="sideMargin">Margem Lateral</label>
                  <input
                    type="text"
                    name="sideMargin"
                    id="sideMargin"
                    placeholder="2,00"
                  />
                </div>
                <div>
                  <label htmlFor="vertical-density">Densidade Vertical</label>
                  <input
                    type="text"
                    name="vertical-density"
                    id="vertical-density"
                    placeholder="2,00"
                  />
                </div>
                <div>
                  <label htmlFor="horizontal-density">
                    Densidade Horizontal
                  </label>
                  <input
                    type="text"
                    name="horizontal-density"
                    id="horizontal-density"
                    placeholder="2,00"
                  />
                </div>
                <div>
                  <label htmlFor="height-tag">Altura</label>
                  <input
                    type="text"
                    name="height-tag"
                    id="height-tag"
                    placeholder="2,00"
                  />
                </div>
                <div>
                  <label htmlFor="width-tah">Largura</label>
                  <input
                    type="text"
                    name="width-tah"
                    id="width-tah"
                    placeholder="2,00"
                  />
                </div>
                <div>
                  <label htmlFor="font">Fonte</label>
                  <select name="font" id="font">
                    <option value="0">Arial</option>
                    <option value="1">Times Nwe Roman</option>
                    <option value="2">Helvetica</option>
                    <option value="3">Times</option>
                    <option value="4">Verdana</option>
                    <option value="5">Courier New</option>
                    <option value="6">Courier</option>
                    <option value="7">Arial Narrow</option>
                    <option value="8">Candara</option>
                    <option value="9">Geneva</option>
                    <option value="10">Calibri</option>
                    <option value="11">Optima</option>
                    <option value="12">Cambria</option>
                    <option value="13">Garamond</option>
                    <option value="14">Perpetua</option>
                    <option value="15">Monaco</option>
                    <option value="16">Didot</option>
                    <option value="17">Brush Script MT</option>
                    <option value="18">Lucida Bright</option>
                    <option value="19">Copperplate</option>
                  </select>
                </div>
                <div>
                  <label htmlFor="font-size-tag">Tamanho Fonte</label>
                  <select name="font-size-tag" id="font-size-tag">
                    <option value="8pt">8pt</option>
                    <option value="9pt">9pt</option>
                    <option value="10pt">10pt</option>
                    <option value="11pt">11pt</option>
                    <option value="12pt">12pt</option>
                    <option value="13pt">13pt</option>
                    <option value="14pt">14pt</option>
                    <option value="15pt">15pt</option>
                    <option value="16pt">16pt</option>
                    <option value="17pt">17pt</option>
                    <option value="18pt">18pt</option>
                    <option value="19pt">19pt</option>
                    <option value="20pt">20pt</option>
                  </select>
                </div>
                <div>
                  <label htmlFor="limit-letters">
                    Limite Caracteres Produto
                  </label>
                  <input
                    type="number"
                    name="limit-letters"
                    id="limit-letters"
                  />
                </div>
                <div>
                  <label htmlFor="page-type">Tipo Página</label>
                  <select name="page-type" id="page-type">
                    <option value="0">A2 - 42,0 X 59,4 cm</option>
                    <option value="1">A1 - 59,4 X 84,1 cm</option>
                    <option value="2">A3 - 29,7 X 42,0 cm</option>
                    <option value="3">A4 - 21,0 X 29,7 cm</option>
                    <option value="4">A5 - 14,8 X 21,0 cm</option>
                    <option value="5">A6 - 10,5 X 14,8 cm</option>
                    <option value="6">A7 - 7,4 X 10,5 cm</option>
                    <option value="7">A8 - 5,2 X 7,4 cm</option>
                    <option value="8">A9 - 3,7 X 5,2 cm</option>
                    <option value="9">A10 - 2,6 X 3,7 cm</option>
                  </select>
                </div>

                {itemsTags.map((item, index) => (
                  <div key={index}>
                    <label htmlFor={item.forNameId}>{item.label}</label>
                    <select name={item.forNameId} id={item.forNameId}>
                      <option value="0">{item.op01}</option>
                      <option value="1">{item.op02}</option>
                    </select>
                  </div>
                ))}
              </form>

              <div className="buttons-tags">
                <SaveButton />
                <BackButton />
              </div>
            </div>
          </Modal>

          <div className="header-table-tags">
            <p>Etiquetas</p>
          </div>
          <ul className="table-tags">
            <tr>
              <td>DEFAULT</td>

              <div className="buttons-table">
                <td>
                  <button className="button-table-yellow">
                    <AiFillStar />
                  </button>
                </td>
                <td>
                  <button className="blue">
                    <BsFillPencilFill />
                  </button>
                </td>
                <td>
                  <button className="red">
                    <BsFillTrashFill />
                  </button>
                </td>
              </div>
            </tr>
          </ul>
        </main>
      </div>
    </ContainerHangTags>
  )
}
