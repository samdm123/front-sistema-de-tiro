import styled from 'styled-components'

export const ContainerHangTags = styled.section`

    background-color: #eee;

    .div-add-ht {
        display: flex;
        width: 76.5vw;
        justify-content: end;

    }

    .add-ht {
        display: flex;
        align-items: center;
        margin-top: 1.5rem;
        margin-left: 2rem;
        justify-content: center;
        width: 12.5rem;
        padding: .5rem;
        text-decoration: none;
        color: white;
        background: #65B951;
        font-size: .8rem;

        transition: .4s;
    }

    .add-ht:hover {
        filter: brightness(110%);
    }

    .header-table-tags {
        width: 75vw;
        padding: .6rem;
        margin: 1.5rem 1.5rem 0 1.5rem;
        color: white;
        background: #65B951;
    }

    .table-tags {
        width: 75vw;
        padding: 0 .6rem;
        margin: 0 1.5rem;
        background: white;
        font-size: .8rem;
    }

    .table-tags > tr {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .table-tags > tr > .buttons-table .blue {
        background: #4285F4;
    }

    .table-tags > tr > .buttons-table .red {
        background: #E74B37;
    }

    .buttons-tags {
        width: 75vw;
        background: white;
        padding: 1rem 2rem;
        margin: 1rem 1.5rem;
        border-radius: .5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
    }

    .buttons-tags .save {
        margin-top: 0;
    }

    .buttons-tags > button {
        border: 0;
        padding: .6rem 1.3rem;
        color: white;
        cursor: pointer;
        text-transform: uppercase;
        transition: .4s;
    }

    .buttons-tags > button:hover {
        filter: brightness(110%);
    }

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;

        header {
            p {
                margin-left: 2rem;
            }
        }
    }
`
