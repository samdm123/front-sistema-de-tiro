import styled from "styled-components"

export const ContainerAddChampionship = styled.section`

    width: 100%;
    padding-left: .5rem;

    .add-champ-header {
        padding: .6rem;
        margin-left: 15rem;
        background: white;
        color: #737373;   
        font-size: 1.3rem;
        font-weight: 300;
    }

    .add-champ {
        display: block;
        width: 78vw;
    }

    input, select {
        opacity: .6;
        height: 2rem;
    }

    label {
            display: block;
            margin-bottom: .5rem;
    }

    .championship {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        align-items: center;
        gap: .5rem;
        label {
            display: block;
        }
        input, select {
            height: 2rem;
        }
    }

    .championship label {
        margin-top: 1rem;
        margin-bottom: .5rem;
    }

    .contentSelectionOptions{
        display: flex;
        flex-direction: column;
    }

    .form-title {
        color: white;
        font-size: .8rem;
        font-weight: normal;
        padding: .6rem;
        background: #11A9CC;
        border-radius: .5rem;
    }

    .integrations-title {
        color: white;
        font-size: .8rem;
        font-weight: normal;
        padding: .6rem;
        background: #11A9CC;
        border-radius: .5rem;

        margin: 2rem 0 1rem 0;
    }

    .integrations {
        label {
            display: block;
            margin-bottom: .5rem;
        }
        input, select {
            height: 2rem;
        }
    }

    .discounts {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        align-items: center;
        gap: 1rem;
    }

    .discounts-title {
        color: white;
        font-size: .8rem;
        font-weight: normal;
        padding: .6rem;
        background: #11A9CC;
        border-radius: .5rem;

        margin: 2rem 0 1rem 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .plus {
        margin: 0;
        /* width: 1.5rem; */
        /* height: 1.2rem; */
        padding: 0;
        font-size: 1.5rem;
        text-decoration: none;
        color: white;
        font-weight: 700;
    }

    .tieBreaks-title {
        color: white;
        font-size: .8rem;
        font-weight: normal;
        padding: .6rem;
        background: #11A9CC;
        border-radius: .5rem;

        margin: 2rem 0 1rem 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .tieBreaks {
        display: flex;
        gap: 1rem;
    }

    .discards-title {
        color: white;
        font-size: .8rem;
        font-weight: normal;
        padding: .6rem;
        background: #11A9CC;
        border-radius: .5rem;

        margin: 2rem 0 1rem 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .discards-champ {
        display: flex;
        gap: 1rem;
    }

    .discards-champ select {
        width: 16rem;
    }

    .buttons-save-back {
        display: flex;
        gap: 1rem;
        margin: 2rem 0;
    }


    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        gap: 1rem;

        header {
            margin-left: 2rem;
        }


        .championship select, .championship input {
            width: 100%;    
        }

        .integrations select {
            width: 100%;
        }

        .discounts select, .discounts input {
            width: 100%;
        }

        .tieBreaks {
            display: flex;
            flex-direction: column;
        }

        .tieBreaks select, .tieBreaks input {
            width: 100%;

        }

        .discards-champ {
            display: flex;
            flex-direction: column;
        }

        .discards-champ select {
            width: 100%;
        }

        

        
    }


`










