import { itemsAddChampionship } from '../../../../services/database'
import SaveButton from '../../../../components/SaveButton'

import {ContainerAddChampionship} from "./styles.js"
import BackButton from '../../../../components/BackButton'

export default function AddChampionship() {
  

  return (
    <ContainerAddChampionship>
      <header className="pages-header">
        <p>Adicionar Campeonato</p>
      </header>

      <div>
        <main className="addChamp-main">
          <form className="add-champ" action="">
            <h3 className="form-title">Campeonato</h3>

            <div className="championship">
              <div>
                <label htmlFor="title-champ">Título</label>
                <input
                  type="text"
                  id="title-champ"
                  placeholder="Ex: Campeonato de Trap Americano"
                />
              </div>

              <div>
                <label htmlFor="season">Temporada</label>
                <select name="season" id="season">

                  <option value="1">Selecione uma temporada</option>
                  <option value="2">2023</option>
                  <option value="3">2022</option>
                  <option value="4">2021</option>
                  <option value="5">2020</option>
                  <option value="6">2019</option>
                  <option value="7">2018</option>
                  <option value="8">2017</option>
                  <option value="9">2016</option>
                  <option value="10">2015</option>
                  <option value="11">2014</option>
                  <option value="12">2013</option>
                  <option value="13">2012</option>
                  <option value="14">2011</option>
                  <option value="15">2010</option>
                  <option value="16">2009</option>
                </select>
              </div>

              {itemsAddChampionship.map((it, index) => (
                <div key={index} className='contentSelectionOptions'>
                  <label htmlFor={it.select}>{it.label}</label>
                  <select name={it.select} id={it.select}>
                    <option value={it.numberValue_0}>
                      {it.contentValue_0}
                    </option>
                    <option value={it.numberValue_1}>
                      {it.contentValue_1}
                    </option>
                    <option value={it?.numberValue_2}>
                      {it?.contentValue_2}
                    </option>
                  </select>
                </div>
              ))}
            </div>

            <h3 className="integrations-title">Integrações</h3>
            <div className="integrations">
              <div>
                <label htmlFor="type-integrations">Tipo:</label>
                <select name="type-integrations" id="type-integrations">
                  <option value="0">CBTP - PROVAS ONLINE</option>
                  <option value="1">SHOOTING HOUSE</option>
                </select>
              </div>
            </div>

            <h3 className="discounts-title">
              Descontos{' '}
              <a href="#" className="plus">
                +
              </a>
            </h3>
            <div className="discounts">
              <div>
                <label htmlFor="percentage-discounts">
                  Percentual de Desconto:
                </label>
                <input type="text" placeholder="50,0000" />
              </div>

              <div>
                <label htmlFor="age-plus">Idade maior que:</label>
                <input type="text" placeholder="Ex: 60" />
              </div>

              <div>
                <label htmlFor="age-minus">Idade menor que:</label>
                <input type="text" placeholder="Ex: 18" />
              </div>

              <div>
                <label htmlFor="type-affiliate">Tipo Filiado:</label>
                <select name="type-affiliate" id="type-affiliate">
                  <option value="0">Todos</option>
                  <option value="1">Somente Filiados</option>
                  <option value="2">Somente Convidados</option>
                  <option value="3">Somente Dependentes</option>
                </select>
              </div>

              <div>
                <label htmlFor="gender">Genêro:</label>
                <select name="gender" id="gender">
                  <option value="0">Ambos</option>
                  <option value="1">Masculino</option>
                  <option value="2">Feminino</option>
                </select>
              </div>

              <div>
                <label htmlFor="Parathlete">Paratleta:</label>
                <select name="Parathlete" id="Parathlete">
                  <option value="0">Indiferente</option>
                  <option value="1">Sim</option>
                  <option value="2">Não</option>
                </select>
              </div>

              <div>
                <label htmlFor="title-discounts">Título:</label>
                <select name="title-discounts" id="title-discounts">
                  <option value="0">Selecione...</option>
                  <option value="1">SÓCIO - ATIVOS</option>
                </select>
              </div>
            </div>

            <h3 className="tieBreaks-title">
              Desempates{' '}
              <a href="#" className="plus">
                +
              </a>
            </h3>
            <div className="tieBreaks">
              <div>
                <label htmlFor="process-tieBreaks">Desempate:</label>
                <select name="process-tieBreaks" id="process-tieBreaks">
                  <option value="0">
                    Selecione o desempate a ser processado
                  </option>
                  <option value="1">Soma de todas as partidas</option>
                  <option value="2">
                    Melhor etapa do peso máx. ao peso min.
                  </option>
                  <option value="3">Da melhor a pior etapa</option>
                  <option value="4">
                    Melhor resultados em etapas que não são descartáveis
                  </option>
                  <option value="5">
                    Melhor resultados nas etapas que são descartáveis
                  </option>
                  <option value="6">
                    Maior quantidade de x em etapas não descartáveis
                  </option>
                  <option value="7">Desempate por peso</option>
                  <option value="8">Melhor na útima etapa do campeonato</option>
                </select>
              </div>

              <div>
                <label htmlFor="order">Ordem:</label>
                <input type="text" id="order" placeholder="Ex: 1" />
              </div>
            </div>

            <h3 className="discards-title">
              Descartes do Campeonato{' '}
              <a href="#" className="plus">
                +
              </a>
            </h3>
            <div className="discards-champ">
              <div>
                <label htmlFor="discards-qtd">Qtd:</label>
                <select name="discards-qtd" id="discards-qtd">
                  <option value="0">
                    Selecione a quantidade a ser descartada
                  </option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                </select>
              </div>

              <div>
                <label htmlFor="discards-weight">
                  Peso das etapas que podem ser descartadas:
                </label>
                <select name="discards-weight" id="discards-weight">
                  <option value="0">
                    Peso das etapas a serem descartadas...
                  </option>
                  <option value="any">QUALQUER PESO</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
              </div>
            </div>

            <h3 className="discards-title">
              Descartes do Campeonato de Equipe{' '}
              <a href="#" className="plus">
                +
              </a>
            </h3>
            <div className="discards-champ">
              <div>
                <label htmlFor="discards-group-qtd">Qtd:</label>
                <select name="discards-group-qtd" id="discards-group-qtd">
                  <option value="0">
                    Selecione a quantidade a ser descartada
                  </option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                </select>
              </div>

              <div>
                <label htmlFor="discards-weight">
                  Peso das etapas que podem ser descartadas:
                </label>
                <select name="discards-weight" id="discards-weight">
                  <option value="0">
                    Peso das etapas a serem descartadas...
                  </option>
                  <option value="any">QUALQUER PESO</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
              </div>
            </div>

            <div className="buttons-save-back">
              <SaveButton />
              <BackButton />
            </div>
          </form>
        </main>
      </div>
    </ContainerAddChampionship>
  )
}
