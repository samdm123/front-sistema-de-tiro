import {AiFillStar, AiFillWarning} from 'react-icons/ai'
import { IoIosArrowDown } from 'react-icons/io'

import {ContainerChampionship} from "./styles.js"
import { Link } from 'react-router-dom'

export default function Championship() {
    return(
        <ContainerChampionship>
            <header className='pages-header'>
                <p>Campeonato</p>
            </header>
           {/* <div>  */}
                    
                <main className='championship-main'>
                    <div className="div-add-cart">
                        <a href="/campeonatos/adicionar" className='add-cart'>
                           + ADICIONAR CAMPEONATO
                        </a>
                    </div>
                    

                    <form action="" className='form-champ' onSubmit={ev => ev.preventDefault()}>
                        <div>
                            <label htmlFor="name-champ">Campeonato</label>
                            <input type="text" id='name-champ' placeholder='Nome do campeonato'/>
                        </div>

                        <div>
                            <label htmlFor="season">Temporada</label>
                            <select name="season" id="season">
                                <option value="1">Todas</option>
                                <option value="2">2023</option>
                                <option value="3">2022</option>
                                <option value="4">2021</option>
                                <option value="5">2020</option>
                                <option value="6">2019</option>
                                <option value="7">2018</option>
                                <option value="8">2017</option>
                                <option value="9">2016</option>
                                <option value="10">2015</option>
                                <option value="11">2014</option>
                                <option value="12">2013</option>
                                <option value="13">2012</option>
                                <option value="14">2011</option>
                                <option value="15">2010</option>
                                <option value="16">2009</option>
                            </select>
                        </div>

                        <div>
                            <label htmlFor="champ-deleted">Campeonato deletado</label>
                            <select name="champ-deleted" id="champ-deleted">
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>

                        <div className="buttons-bot-champ">
                            <button type='reset' className='clear-filter'>Limpar filtro</button>
                            <button type="submit" className='filter-btn'>Filtrar</button>
                        </div>
                        
                    </form>

                    <div className="table-filtered">
                        <ul>
                            <thead className='head-table'>
                                <tr>
                                    <th>CAMPEONATO</th>
                                    <th>TEMPORADA</th>
                                </tr>
                            </thead>

                            <tbody className='body-table'>
                                <tr>
                                    <td>CAMPEONATO DE TIRO - ICT</td>
                                    <td>2021</td>
                                    <div className='buttons-table'>
                                        <td><button className='button-table-yellow'><IoIosArrowDown/></button></td>
                                    </div>
                                </tr>
                                
                            </tbody>
                            
                        </ul>
                        
                    </div>

                    <div className="warning disable">
                        <AiFillWarning /> 
                        Você não possui nenhum dado cadastrado.
                    </div>
                </main>
            {/* </div>    */}
        </ContainerChampionship>
    )
}