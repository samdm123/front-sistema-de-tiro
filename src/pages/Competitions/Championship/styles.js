
import styled from 'styled-components'

export const ContainerChampionship = styled.section`
    display: flex;
    flex-direction: column;
    width: 100%;
    background-color: #eee;


    .div-add-cart {
        display: flex;
        width: 76.5vw;
        justify-content: end;
    }

     .add-cart {
        display: flex;
        align-items: center;
        margin-top: 1.5rem;
        margin-left: 2rem;
        justify-content: center;
        width: 12.5rem;
        padding: .5rem;
        text-decoration: none;
        color: white;
        background: #65B951;
        font-size: .8rem;

        transition: .4s;
    } 

     .add-cart:hover {
        filter: brightness(110%);
    }

    label {
        display: block;
        font-size: .8rem;
        padding-bottom: .3rem;
    }

    form {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        width: 75vw;

        margin-top: 1.6rem;
        background: white;
        padding: 1.5rem;
        border-radius: .5rem;
        margin-left: 1.5rem;
        box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);

    }

    form input {
        padding: .4rem;
        opacity: .6;
        width: 15rem;
    }

    form select {
        padding: .4rem;
        opacity: .6;
        width: 10rem;
    }

    form button {
        color: white;
        width: 10rem;
        border: 0;
        margin-top: 1.3rem;
        padding: .5rem;
        cursor: pointer;

        transition: .4s;
    }

    form button:hover {
        filter: brightness(110%);
    }

    .clear-filter {
        background: #F6C12A;
    }

    .filter-btn {
        background: #65B951;
    }

    .warning {
        margin-top: 4rem;
        width: 64rem;
        margin-left: 1.5rem;
        padding: .8rem;
        background: #FFF1A8;
        border-radius: .5rem;
        font-weight: 300;
        display: flex;
        align-items: center;
        gap: .5rem;
    }

    .disable {
        display: none;
    }


    .buttons-bot-champ {
        display: flex;
        gap: 1rem;
    }


    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
        align-items: center;

        header {
            margin-left: 2rem;
        }

        .div-add-cart {
            display: flex;
        }

        .form-champ {
            display: block;
            justify-content: space-between;
            align-items: center;
            width: 75vw;

            background: white;
            padding: 1.5rem;
            border-radius: .5rem;
            margin-left: 1.5rem;
            box-shadow: 0 0 .3em rgba(0, 0, 0, 0.363);
        
        }

        .form-champ input {
            padding: .4rem;
            opacity: .6;
            width: 100%;
            margin-bottom: 1rem;
        }

        .form-champ select {
            padding: .4rem;
            opacity: .6;
            width: 100%;
            margin-bottom: 1rem;
        }

    }

    @media (max-width: 525px) {
        .buttons-table {
            button {
                margin-top: .4rem;
            }
        }
    }


`
