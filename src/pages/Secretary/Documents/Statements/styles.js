import styled from 'styled-components'

export const ContainerStatements = styled.div`
  display: flex;

  .container {
    /* border: 1px solid red; */
    width: 100%;
    height: 100%;

    display: flex;
    flex-direction: column;
    padding-inline: 0.5rem;
  }
  .container header {
    width: 100%;
    background: rgb(228, 225, 225);
    padding: 1rem;
    border-radius: 0.24rem;
  }

  .container header svg {
    margin-right: 0.5rem;
  }
  .container header a {
    margin-right: 1rem;
    color: black;
    text-decoration: none;
    transition: 0.3s ease;
  }

  .container header a:hover {
    color: #138496;
  }

  .container header a:visited,
  .container headera a:active {
    color: black;
  }

  /*  =================================conteudo do form */

  .wrapperStatement {
    width: 100%;
  }

  .containerGridColum {
    display: grid;
    grid-template-columns: repeat(4, 1fr);

    /* place-items: center; */
    align-items: center;
    column-gap: 0.5rem;
    row-gap: 2rem;

    padding-block: 2rem;
  }
  .formStatement {
    padding-inline: 1rem;
  }

  .formStatement label {
    font-size: 1rem;
  }

  .containerGridColum .contentStatemetent {
    max-width: 20rem;
  }

  .containerGridColum .contentStatemetent input {
    width: 100%;

    font-size: 1rem;
  }

  .containerGridColum .contentStatemetent select {
    width: 100%;
  }

  .containerGridColum .contentStatemetent input:focus,
  .containerGridColum .contentStatemetent select:focus {
    color: #000;
    outline: 1px solid #138496;
  }

  .containerStatementAndButtons {
    margin: 0 auto;
    width: 100%;

    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    

    /* border: 1px solid green; */
    margin-bottom: 1rem;
  }

  .typeOfStatement {
    /* border: 1px solid red; */
    max-width: 300px;
  }

  .typeOfStatement select {
    width: 100%;
    font-size: 1rem;
  }

  .buttonsStatement {
    /* width: 100%; */
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-end;
    align-items: flex-end;
    /* border: 1px solid red; */
  }

  .buttonsStatement button {
    border: 1px solid rgba(128, 128, 128, 0.377);
    border-radius: 0.5rem;
    text-transform: capitalize;

    display: flex;
    justify-content: center;
    align-items: center;

    width: 6rem;
    height: 2rem;
    color: #fff;
    box-shadow: 3px 5px 8px rgba(0,0,0,0.3);


    &:hover{
      filter: brightness(.8);
    }
  }

  .buttonsStatement button svg {
    margin-right: 0.5rem;
  }

  .buttonsStatement button + button {
    margin-left: 1rem;
  }

  .buttonClear {
    background-color: #c82333;
    /* color: white !important; */
    margin: 0;
  }

  .buttonFilter {
    background-color: #218838;
    color: white;
    margin: 0;
  }

  .containerThreeButtons {
    /* border: 1px solid blue; */
    width: 100%;

    display: grid;
    grid-template-columns: 1fr 1fr;
    justify-content: space-between;
    align-items: center;
    column-gap: 0.5rem;
  }

  .containerThreeButtons .buttonShowDelete {
    /* padding-inline: 3rem; */
    border: 1px solid #c82333;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 320px;
    height: 2.3rem;
    color: #c82333;
    border-radius: 0.25rem;
    transition: .2s ease;
  }
  .containerThreeButtons .buttonShowDelete svg {
    margin-right: 0.5rem;
    color: #c82333;

    transition: 0.3s ease;
  }

  .containerThreeButtons .buttonShowDelete:hover {
    background-color: #c82333;
    color: white;
  }

  .containerThreeButtons .buttonShowDelete:hover svg {
    color: white;
  }

  .containerButtons {
    /* border: 1px solid red; */
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: flex-end;
    flex-wrap: wrap;
    gap: 0.5rem;
  }

  .buttonReport {
    background-color: #11a9cc;
    text-transform: uppercase;
    border-radius: 0.23rem;
    width: 250px;
    height: 2.3rem;

    display: flex;
    justify-content: center;
    align-items: center;

    transition: .2s ease;
    color: #fff;
    box-shadow: 3px 5px 8px rgba(0,0,0,0.3);


    svg{
      margin-right: .5rem;
    }

    &:hover{
      filter: brightness(.8);
    }
  }

  .buttonStatement {
    background-color: #24963e;
    text-transform: uppercase;
    border-radius: 0.23rem;
    width: 250px;
    height: 2.3rem;

    display: flex;
    justify-content: center;
    align-items: center;
    color: #fff;
    box-shadow: 3px 5px 8px rgba(0,0,0,0.3);


    svg{
      margin-right: .5rem;
    }

    &:hover{
      filter: brightness(.8);
    }
  }

  .containeStatementsIssued {
    width: 100%;
    height: 5rem;
    display: flex;
    flex-direction: column;
    margin-top: 1rem;
  }

  .contentStatemetentIssued {
    display: flex;
    justify-content: center;
    align-items: center;

    overflow: auto;
  }

  .containeStatementsIssued a {
    font-size: 0.8rem;
    display: inline-block;
    text-align: center;
    color: black;
    text-decoration: none;

    transition: 0.2s ease;
  }

  .containeStatementsIssued a:hover {
    color: #11a9cc;
  }

  .containeStatementsIssued strong {
    padding: 0.3rem;
    border-radius: 0.25rem;
    color: white;
    background-color: #17a2b8;
  }

  .contentStatemetentIssued,
  .start {
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: #ccd0d1;
  }

  .start {
    gap: 1rem;
  }

  .contentStatemetentIssued ul {
    display: flex;
    align-items: center;
    gap: 1rem;
    /* border:   1px solid red; */
  }

  .contentStatemetentIssued ul li {
    list-style: none;
  }

  .contentStatemetentIssued ul li a {
    display: inline-block;
    text-align: center;
    color: black;
    text-decoration: none;
  }

  /*  =============== MEDIA QUERY */

  @media (max-width: 1130px) {
    .containerGridColum {
      grid-template-columns: repeat(3, 1fr);
    }
  }

  @media (max-width: 970px) {
    .containerGridColum {
      grid-template-columns: repeat(2, 1fr);

      row-gap: 1rem;
    }

    /* .buttonsStatement{
      flex-direction: column;
    } */
  }
  @media (max-width: 890px) {
    .containerButtons {
      flex-direction: column;
      /* justify-content: center; */
      align-items: center;
      gap: 0;
    }
  }

  @media (max-width: 700px) {
    .containerGridColum {
      grid-template-columns: 1fr;

      place-items: center;
    }

    .containerStatementAndButtons {
      display: grid;
      grid-template-columns: 1fr;
      place-items: center;
      gap: 0.5rem;
    }

    .containerThreeButtons {
      grid-template-columns: 1fr;
      place-items: center;
    }

    .containerButtons {
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
  }
`
