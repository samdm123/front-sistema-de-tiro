import Header from '../../../../components/Header'
import { Sidebar } from '../../../../components/Sidebar'
import { HiDocument } from 'react-icons/hi'
import { FaHome } from 'react-icons/fa'
import {FiSearch} from "react-icons/fi"
import {BiEraser} from 'react-icons/bi'
import {BsEyeFill} from "react-icons/bs"
import {FcViewDetails} from "react-icons/fc"
import {AiOutlinePlus} from "react-icons/ai"
import { selectOptions } from '../../../../services/database'


import './styles.js'

import {ContainerStatements} from './styles.js'

export  function Statements() {
  return (
    <ContainerStatements>
      
      <div className="container">
        <header>
          <a href="#">
            <FaHome />
            Home
          </a>
          <span>
            <HiDocument />
            Declarações{' '}
          </span>
        </header>

        <div className="wrapperStatement">
          <form action="" className="formStatement" onSubmit={ev => ev.preventDefault()}>

          <div className="containerGridColum">
            <div className="contentStatemetent">
              <label htmlFor="name">NOME/MATRÍCULA</label>
              <input
                type="text"
                name="name"
                id="name"
                placeholder="Nome da matrícula"
              />
            </div>

            <div className="contentStatemetent">
              <label htmlFor="id">ID/SIST.</label>
              <input
                type="text"
                name="id"
                id="id"
                placeholder="Id do sistema"
              />
            </div>

            <div className="contentStatemetent">
              <label htmlFor="MAT.INT">MAT.INT</label>
              <input
                type="text"
                name="MAT.INT"
                id="MAT.INT"
                placeholder="Mat. Int"
              />
            </div>

            <div className="contentStatemetent">
              <label htmlFor="date">VALIDADE</label>
              <input type="date" name="date" id="date" />
            </div>

            <div className="contentStatemetent">
              <label htmlFor="date">VALIDADE ATÉ</label>
              <input type="date" name="date" id="date" />
            </div>

            <div className="contentStatemetent">
              <label htmlFor="status">STATUS</label>
              <select name="" id="" className="contentStatemetent">
                <option value="todos">TODOS</option>
                <option value="aguardando liberação">
                  AGUARDANDO LIBERAÇÃO
                </option>
                <option value="aguardando lote">AGUARDANDO LOTE</option>
                <option value="aguardando assinatura">
                  AGUARDANDO ASSINATURA
                </option>
                <option value="aprovada">APROVADA</option>
                <option value="rejeitada">REJEITADA</option>
              </select>
            </div>

            <div className="contentStatemetent">
              <label htmlFor="date">VALIDADE</label>
              <input type="date" name="date" id="date" />
            </div>

            <div className="contentStatemetent">
              <label htmlFor="date">VALIDADE</label>
              <input type="date" name="date" id="date" />
            </div>
          </div>

            

            <div className="containerStatementAndButtons">

                <div className="typeOfStatement">
                    <label htmlFor="tipo declaração">TIPO DE DECLARAÇÃO</label>
                    <select name="" id="tipo declaração" className="contentStatemetent">
                      
                      {selectOptions.map( option => (
                        <option key={option.value} value={option.value}>{option.value}</option>
                      ))}
                    </select>
                </div>

                <div className="buttonsStatement">
                  <button className='buttonClear'>
                    <BiEraser/> limpar
                  </button>

                  <button className='buttonFilter'>
                    <FiSearch/>
                    Filtrar
                  </button>
                </div>

            </div>

            <div className="containerThreeButtons">

               <button className='buttonShowDelete'>
                  <BsEyeFill/>    MOSTRAR DELETADOS
               </button>


                <div className="containerButtons">
                  <button className='buttonReport'>
                    <FcViewDetails/> Relatórios
                  </button>

                  <button className='buttonStatement'>
                    <AiOutlinePlus/> gerar declaração
                  </button>
                </div>

            </div>

            
            
          </form>
        </div>
        

        <div className="containeStatementsIssued">
          <strong>Declarações Emitidas - 0</strong>


          <div className='contentStatemetentIssued'>
              <div className="start">
                <a href="#"># {' '} MAT</a>
                <a href="#">FILIADO</a>
              </div>


              <a href="#">DECLARAÇÃO</a>

              <ul>
                <li><a href="#">EMISSÃO</a></li>
                <li><a href="#">VALIDADE</a></li>
                <li><a href="#">SOLICITADOR POR</a></li>
                <li><a href="#">APROVADA</a></li>
                <li><a href="#">STATUS</a></li>
              </ul>
          </div>

        </div>
      </div>
    </ContainerStatements>
  )
}
