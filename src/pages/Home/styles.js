import styled from 'styled-components'

export const DashboardContainer = styled.div`
  display: flex;
  flex-direction: column;
  
  width: 100%;


  .dash-header {
    padding: 0.6rem;
    color: #737373;
    font-size: 1.3rem;
    font-weight: 300;
    /* border: 1px solid red; */
  }

  .dashboard-op {
    width: 100%;
    /* height:30rem; */
    background: #eeeeee;
    /* border: 1px solid red; */

  }

  .options {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-auto-rows: 50px;
    gap: 1rem;
    background: #eeeeee;
    padding: 2rem;
    height: auto;
  }

  .buttons-nav {
    background: #ffe;
    height: 3rem;
    box-shadow: 0 0 0.1em #000;
    border-radius: 0.5rem;
    text-decoration: none;
    color: #000;
    font-weight: 400;
    font-size: 0.7rem;
    display: flex;
    align-items: center;
    gap: 1rem;
    transition: 0.3s;
    /* width: 100%; */
  }

  .buttons-nav-txt {
    text-decoration: none;
    color: #22afd0;
  }

  .buttons-nav:hover {
    opacity: 0.8;
  }

  .dash-bi {
    background: #eeeeee;
    display: flex;
    justify-content: center;
    gap: 2rem;
  }

  .graphic {
    padding: 5rem;
    background: #fff;
    margin-bottom: 1rem;
    /* border:1px solid red; */
  }

  /* ========== BUTTONS DAS OPÇOES ========== */

  .red {
    text-decoration: none;
    font-size: 1.4rem;
    background: #d73d32;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
  }

  .green {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #8cc474;
  }

  .blue {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #4374e0;
  }

  .green-dark {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #53a93f;
  }

  .blue-light {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #57b5e3;
  }

  .yellow {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #f4b400;
  }

  .purple {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #7e3794;
  }

  .pink {
    text-decoration: none;
    font-size: 1.4rem;
    padding: 0.6rem;
    border-radius: 0.5rem 0 0 0.5rem;
    color: white;
    background: #e75b8d;
  }

  /* ===================== */
`
