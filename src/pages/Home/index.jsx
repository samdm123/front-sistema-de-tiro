
import ButtonOptions from '../../components/ButtonOptions'
import { AiOutlineWifi } from 'react-icons/ai'
import { FaUser } from 'react-icons/fa'
import { IoDocumentsSharp } from 'react-icons/io5'
import { IoMdSchool } from 'react-icons/io'
import { IoDocumentSharp } from 'react-icons/io5'
import { BsArrowsAngleExpand } from 'react-icons/bs'
import { CgNotes } from 'react-icons/cg'
import { FaBarcode } from 'react-icons/fa'
import { BsListTask } from 'react-icons/bs'
import { BsFillCartFill } from 'react-icons/bs'

import { DashboardContainer } from './styles'

export function Home() {
  return (
    <>
      <DashboardContainer>
        <div className="dash-header">Dashboard</div>

        <div className="dashboard-op">
          <div className="options">
            <ButtonOptions
              url="#"
              icon={<AiOutlineWifi />}
              title="BLOG SHOOTING HOUSE"
              classColor="red"
            />
            <ButtonOptions
              url="#"
              icon={<FaUser />}
              title="FECHAR CAIXA"
              classColor="green"
            />
            <ButtonOptions
              url="#"
              icon={<IoDocumentsSharp />}
              title="ADICIONAR DECLARAÇÃO"
              classColor="blue"
            />
            <ButtonOptions
              url="#"
              icon={<IoDocumentsSharp />}
              title="ADICIONAR SOLICITAÇÃO DESPACHANTE"
              classColor="green-dark"
            />
            <ButtonOptions
              url="#"
              icon={<FaUser />}
              title="ADICIONAR CONVIDADO"
              classColor="blue"
            />
            <ButtonOptions
              url="#"
              icon={<IoMdSchool />}
              title="TREINAMENTOS"
              classColor="blue"
            />
            <ButtonOptions
              url="#"
              icon={<FaUser />}
              title="BUSCAR ATLETA"
              classColor="blue-light"
            />
            <ButtonOptions
              url="#"
              icon={<IoDocumentSharp />}
              title="NOTAS FISCAIS"
              classColor="yellow"
            />
            <ButtonOptions
              url="#"
              icon={<BsArrowsAngleExpand />}
              title="ADICIONAR ENTRADA FLUXO DE CAIXA"
              classColor="green"
            />
            <ButtonOptions
              url="#"
              icon={<BsArrowsAngleExpand />}
              title="TRANSFERÊNCIA FLUXO DE CAIXA"
              classColor="blue-light"
            />
            <ButtonOptions
              url="#"
              icon={<FaUser />}
              title="ADICIONAR FILIADO"
              classColor="yellow"
            />
            <ButtonOptions
              url="#"
              icon={<CgNotes />}
              title="CRs VENCIDOS"
              classColor="red"
            />
            <ButtonOptions
              url="#"
              icon={<FaBarcode />}
              title="ADICIONAR ANUIDADE EM MASSA"
              classColor="blue"
            />
            <ButtonOptions
              url="#"
              icon={<BsListTask />}
              title="ADICIONAR HABITUALIDADE"
              classColor="purple"
            />
            <ButtonOptions
              url="#"
              icon={<FaUser />}
              title="ADICIONAR VISITA"
              classColor="purple"
            />
            <ButtonOptions
              url="#"
              icon={<CgNotes />}
              title="ADICIONAR LAUDO"
              classColor="pink"
            />
            <ButtonOptions
              url="#"
              icon={<IoDocumentSharp />}
              title="DOCUMENTO PENDENTE"
              classColor="yellow"
            />
            <ButtonOptions
              url="#"
              icon={<CgNotes />}
              title="ADICIONAR CONTA A PAGAR"
              classColor="purple"
            />
            <ButtonOptions
              url="#"
              icon={<BsArrowsAngleExpand />}
              title="ADICIONAR SAÍDA FLUXO DE CAIXA"
              classColor="red"
            />
            <ButtonOptions
              url="#"
              icon={<FaBarcode />}
              title="ADICIONAR BOLETO"
              classColor="pink"
            />
            <ButtonOptions
              url="#"
              icon={<BsFillCartFill />}
              title="ADICIONAR PEDIDO"
              classColor="red"
            />
          </div>

          <div className="dash-bi">
            <div className="graphic"></div>
            <div className="graphic"></div>
          </div>
        </div>
      </DashboardContainer>
    </>
  )
}
